#pragma once
#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>
#include <yaml-cpp/yaml.h>

#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <cctype>
#include <cstdint>
#include <exception>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "log.h"
#include "thread.h"
#include "util.h"
namespace sylar {
class ConfigVarbase {
public:
    typedef std::shared_ptr<ConfigVarbase> ptr;
    ConfigVarbase(const std::string& name, const std::string& description = "")
        : m_name(name), m_description(description) {
        std::transform(m_name.begin(), m_name.end(), m_name.begin(),
                       [](unsigned char c) { return std::tolower(c); });
    }
    virtual ~ConfigVarbase() {}
    const std::string& getName() const { return m_name; }
    const std::string& getDescription() const { return m_description; }
    virtual std::string toString() = 0;
    virtual bool fromString(const std::string& val) = 0;
    virtual std::string getTypeName() const = 0;

protected:
    std::string m_name;
    std::string m_description;
};

// F from_type,T to_type
template <class F, class T>
class LexicalCast {
public:
    T operator()(const F& v) { return boost::lexical_cast<T>(v); }
    F operator()(const T& v) { return boost::lexical_cast<T>(v); }
};

static std::string trim(const std::string& str) {
    size_t first = str.find_first_not_of(" \t\r\n");
    size_t last = str.find_last_not_of(" \t\r\n");
    if (first == std::string::npos || last == std::string::npos) {
        return "";
    }
    return str.substr(first, last - first + 1);
}

template <class T>
class LexicalCast<std::string, std::vector<T>> {
public:
    std::vector<T> operator()(const std::string& v) {
        std::vector<T> vec;
        std::string document;
        YAML::Node node = YAML::Load(v);
        for (std::size_t i = 0; i < node.size(); ++i) {
            std::stringstream ss;
            ss << node[i];
            vec.push_back(LexicalCast<std::string, T>()(ss.str()));
        }

        return vec;
    }
};

template <class T>
class LexicalCast<std::vector<T>, std::string> {
public:
    std::string operator()(const std::vector<T>& v) {
        YAML::Node node;
        for (auto& i : v) {
            node.push_back(LexicalCast<T, std::string>()(i));
        }
        std::stringstream ss;
        ss << node;
        return ss.str();
    }
};

template <class T>
class LexicalCast<std::string, std::list<T>> {
public:
    std::list<T> operator()(const std::string& v) {
        YAML::Node node = YAML::Load(v);
        std::list<T> l;
        for (size_t i = 0; i < node.size(); ++i) {
            l.push_back(
                LexicalCast<std::string, T>()(node[i].as<std::string>()));
        }
        return l;
    }
};

template <class T>
class LexicalCast<std::list<T>, std::string> {
public:
    std::string operator()(const std::list<T>& v) {
        YAML::Node node;
        for (auto& i : v) {
            node.push_back(LexicalCast<T, std::string>()(i));
        }
        std::stringstream ss;
        ss << node;
        return ss.str();
    }
};

template <class T>
class LexicalCast<std::string, std::set<T>> {
public:
    std::set<T> operator()(const std::string& v) {
        YAML::Node node = YAML::Load(v);
        std::set<T> set;
        for (size_t i = 0; i < node.size(); ++i) {
            set.insert(
                LexicalCast<std::string, T>()(node[i].as<std::string>()));
        }
        return set;
    }
};

template <class T>
class LexicalCast<std::set<T>, std::string> {
public:
    std::string operator()(const std::set<T>& v) {
        YAML::Node node;
        for (auto& i : v) {
            node.push_back(LexicalCast<T, std::string>()(i));
        }
        std::stringstream ss;
        ss << node;
        return ss.str();
    }
};

template <class T>
class LexicalCast<std::string, std::unordered_set<T>> {
public:
    std::unordered_set<T> operator()(const std::string& v) {
        YAML::Node node = YAML::Load(v);
        std::unordered_set<T> uset;
        for (size_t i = 0; i < node.size(); ++i) {
            uset.insert(
                LexicalCast<std::string, T>()(node[i].as<std::string>()));
        }
        return uset;
    }
};

template <class T>
class LexicalCast<std::unordered_set<T>, std::string> {
public:
    std::string operator()(const std::unordered_set<T>& v) {
        YAML::Node node;
        for (auto& i : v) {
            node.push_back(LexicalCast<T, std::string>()(i));
        }
        std::stringstream ss;
        ss << node;
        return ss.str();
    }
};

template <class T>
class LexicalCast<std::string, std::map<std::string, T>> {
public:
    std::map<std::string, T> operator()(const std::string& v) {
        YAML::Node node = YAML::Load(v);
        std::map<std::string, T> mp;
        std::stringstream ss;
        for (auto it = node.begin(); it != node.end(); ++it) {
            ss.str("");
            ss << it->second;
            mp.insert(std::make_pair(it->first.Scalar(),
                                     LexicalCast<std::string, T>()(ss.str())));
        }
        return mp;
    }
};

template <class T>
class LexicalCast<std::map<std::string, T>, std::string> {
public:
    std::string operator()(const std::map<std::string, T>& v) {
        YAML::Node node;
        for (auto& i : v) {
            node[i.first] = YAML::Load(LexicalCast<T, std::string>()(i.second));
        }
        std::stringstream ss;
        ss << node;
        return ss.str();
    }
};

template <class T>
class LexicalCast<std::string, std::unordered_map<std::string, T>> {
public:
    std::unordered_map<std::string, T> operator()(const std::string& v) {
        YAML::Node node = YAML::Load(v);
        std::unordered_map<std::string, T> mp;
        std::stringstream ss;
        for (auto it = node.begin(); it != node.end(); ++it) {
            ss.str("");
            ss << it->second;
            mp.insert(std::make_pair(it->first.Scalar(),
                                     LexicalCast<std::string, T>()(ss.str())));
        }
        return mp;
    }
};

template <class T>
class LexicalCast<std::unordered_map<std::string, T>, std::string> {
public:
    std::string operator()(const std::unordered_map<std::string, T>& v) {
        YAML::Node node;
        for (auto& i : v) {
            node[i.first] = YAML::Load(LexicalCast<T, std::string>()(i.second));
        }
        std::stringstream ss;
        ss << node;
        return ss.str();
    }
};

// FromStr T operator(const std::string&)
// ToStr std::string operator()(const T&)
template <class T, class FromStr = LexicalCast<std::string, T>,
          class ToStr = LexicalCast<T, std::string>>
class ConfigVar : public ConfigVarbase {
public:
    typedef RWMutex RWMutexType;
    typedef std::shared_ptr<ConfigVar> ptr;
    typedef std::function<void(const T& old_value, const T& new_value)>
        on_change_cb;
    ConfigVar(const std::string& name, const T& default_value,
              const std::string& description = "")
        : ConfigVarbase(name, description), m_val(default_value) {}
    std::string toString() override {
        try {
            RWMutexType::ReadLock lock(m_mutex);
            return ToStr()(m_val);
        } catch (std::exception& ex) {
            SYLAR_LOG_ERROR(SYLAR_LOG_ROOT())
                << "ConfigVar::toString exception" << ex.what()
                << " convert: " << typeid(m_val).name() << " to-string";
        }
        return "";
    }
    bool fromString(const std::string& val) override {
        try {
            setValue(FromStr()(val));
        } catch (std::exception& ex) {
            SYLAR_LOG_ERROR(SYLAR_LOG_ROOT())
                << "ConfigVar::toString exception" << ex.what()
                << " convert: " << typeid(m_val).name()
                << " from-string: " << val;
            return false;
        }
        return true;
    }
    const T getValue() {
        RWMutexType::ReadLock lock(m_mutex);
        return m_val;
    }
    void setValue(const T& v) {
        {
            RWMutexType::ReadLock lock(m_mutex);
            if (v == m_val) {
                return;
            }
            for (auto& i : m_cbs) {
                i.second(m_val, v);
            }
        }
        RWMutexType::WriteLock lock(m_mutex);
        m_val = v;
    }
    std::string getTypeName() const override { return typeid(T).name(); }
    uint64_t addListener(uint64_t key, on_change_cb cb) {
        static uint64_t s_fun_id = 0;
        RWMutexType::WriteLock lock(m_mutex);
        ++s_fun_id;
        m_cbs[s_fun_id] = cb;
        return s_fun_id;
    }
    void delListener(uint64_t key) {
        RWMutexType::WriteLock lock(m_mutex);
        m_cbs.erase(key);
    }
    on_change_cb getListener(uint64_t key) {
        RWMutexType::ReadLock lock(m_mutex);
        auto it = m_cbs.find(key);
        return it == m_cbs.end() ? nullptr : it->second;
    }

    void clearListener() { m_cbs.clear(); }

private:
    RWMutexType m_mutex;
    T m_val;
    // 变更回调数组,uint64_t key,要求唯一，一般可以用hash
    std::map<uint64_t, on_change_cb> m_cbs;
};

class Config {
public:
    typedef std::unordered_map<std::string, ConfigVarbase::ptr> configVarMap;
    typedef RWMutex RWMutexType;
    // typedef std::map<std::string, ConfigVarbase::ptr> configVarMap;
    template <class T>
    static typename ConfigVar<T>::ptr Lookup(
        const std::string& name, const T& default_value,
        const std::string& description = "") {
        RWMutexType::WriteLock lock(s_mutex);
        auto it = m_datas.find(name);
        if (it != m_datas.end()) {
            auto tmp = std::dynamic_pointer_cast<ConfigVar<T>>(it->second);
            if (tmp) {
                SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
                    << "Lookup name = " << name << " exists";
                return tmp;
            } else {
                SYLAR_LOG_ERROR(SYLAR_LOG_ROOT())
                    << "Lookup name=" << name << " exists but type not"
                    << typeid(T).name()
                    << " real_type=" << it->second->getTypeName();
                return nullptr;
            }
        }
        if (name.find_first_not_of("abcdefghijklmnopqrstuvwxyz._0123456789") !=
            std::string::npos) {
            SYLAR_LOG_ERROR(SYLAR_LOG_ROOT())
                << " Lookup name invalid " << name;
            throw std::invalid_argument(name);
        }
        typename ConfigVar<T>::ptr v(
            new ConfigVar<T>(name, default_value, description));
        m_datas[name] = v;
        return v;
    }
    template <class T>
    static typename ConfigVar<T>::ptr Lookup(const std::string& name) {
        RWMutexType::ReadLock lock(s_mutex);
        auto it = m_datas.find(name);
        if (it == m_datas.end()) {
            return nullptr;
        }
        return std::dynamic_pointer_cast<ConfigVar<T>>(it->second);
    }
    static void LoadFromYaml(const YAML::Node& root);
    static ConfigVarbase::ptr LookupBase(const std::string& name);
    static void Visit(std::function<void(ConfigVarbase::ptr)> cb);

private:
    // static configVarMap& GetDatas() {
    inline static configVarMap m_datas;
    // return m_datas;
    // }
    inline static RWMutexType s_mutex;
};

}  // namespace sylar