#pragma once
#include <sys/ucontext.h>
#include <ucontext.h>

#include <cstdint>
#include <memory>

#include "fiber.h"
// #include "scheduler.h"
#include "thread.h"

namespace sylar {
class Scheduler;

class Fiber : public std::enable_shared_from_this<Fiber> {
    friend class Scheduler;

public:
    typedef std::shared_ptr<Fiber> ptr;
    // 定义协程状态
    enum State {
        INIT,   // 初始化状态
        HOLD,   // 暂停状态
        EXEC,   // 执行状态
        TERM,   // 结束状态
        READY,  // 就绪状态
        EXCEP,  // 异常状态
    };

private:
    Fiber();

public:
    Fiber(std::function<void()> cb, size_t stacksize = 0,
          bool use_caller = false);
    ~Fiber();
    // 重置协程函数，并重置状态
    // 仅在 INIT 或 TERM 状态下有效
    void reset(std::function<void()> ob);
    // 切换到当前协程执行
    void swapIn();
    // 切换到后台
    void swapOut();

    void call();
    void back();

    uint64_t getId() const { return m_id; }

public:
    // 设置当前协程
    static void SetThis(Fiber* f);
    // 返回当前执行点的协程
    static Fiber::ptr GetThis();
    // 协程切换到后台并设置为ready状态
    static void YieldToRead();
    // 协程切换到后台，并设置为Hold状态
    static void YieldToHold();
    // 总协程数
    static uint64_t TotalFibers();
    static void MainFunc();
    static void CallerMainFunc();
    static uint64_t GetFiberId();
    State getState() { return m_state; }

private:
    uint64_t m_id = 0;
    uint32_t m_stacksize = 0;
    State m_state = INIT;

    ucontext_t m_ctx;
    void* m_stack = nullptr;
    std::function<void()> m_cb;
};
}  // namespace sylar