#if 0
#include <arpa/inet.h>
#include <asm-generic/errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <cerrno>
#include <cstring>

#include "iomanager.h"
#include "log.h"
#include "timer.h"
#include "util.h"

sylar::Logger::ptr g_logger = SYLAR_LOG_ROOT();
void test_fiber() { SYLAR_LOG_INFO(g_logger) << "test_fiber:"; }

void test1() {
    sylar::IOManager iom;
    iom.schedule(&test_fiber);
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    fcntl(sock, F_SETFL, O_NONBLOCK);
    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(80);
    inet_pton(AF_INET, "39.156.66.14", &addr.sin_addr.s_addr);
    if (!connect(sock, (sockaddr*)&addr, sizeof(addr))) {
    } else if (errno == EINPROGRESS) {
        iom.addEvent(sock, sylar::IOManager::READ,
                     []() { SYLAR_LOG_INFO(g_logger) << "read callback"; });
        iom.addEvent(sock, sylar::IOManager::WRITE, [sock]() {
            SYLAR_LOG_INFO(g_logger) << "write callback";
            sylar::IOManager::GetThis()->cancelEvent(sock,
                                                     sylar::IOManager::READ);
            close(sock);
        });
    } else {
        SYLAR_LOG_INFO(g_logger) << "else " << errno << " " << strerror(errno);
    }
}

void test() {
    std::cout << "EPOLLIN=" << EPOLLIN << " EPOLLOUT=" << EPOLLOUT << std::endl;
    sylar::IOManager iom(2, false);
    iom.schedule(&test_fiber);
}
sylar::Timer::ptr s_timer;
void test_timer() {
    sylar::IOManager iom(2);
    s_timer = iom.addTimer(
        1000,
        []() {
            static int i = 0;
            SYLAR_LOG_INFO(g_logger) << "hello timer " << i;
            if (++i == 5) {
                // s_timer->cancel();
                s_timer->reset(2000, true);
            }
        },
        true);
}

int main() { test_timer(); }
#endif