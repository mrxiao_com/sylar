#pragma once
#include <functional>
#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include "fiber.h"
#include "thread.h"

namespace sylar {

class Scheduler {
public:
    typedef std::shared_ptr<Scheduler> ptr;
    typedef Mutex MutexType;

    // 构造函数
    Scheduler(size_t threads = 1, bool use_caller = true,
              const std::string& name = "");

    // 析构函数
    virtual ~Scheduler();

    // 获取调度器名称
    const std::string& getName() const { return m_name; }

    // 静态方法：获取当前线程关联的调度器
    static Scheduler* GetThis();

    // 静态方法：获取当前线程关联的主要Fiber
    static Fiber* GetMainFiber();

    // 启动调度器
    void start();

    // 停止调度器
    void stop();

    // 调度一个Fiber或回调函数执行
    template <class FiberOrCb>
    void schedule(FiberOrCb fc, int thread = -1) {
        bool need_tickle = false;
        {
            MutexType::Lock lock(m_mutex);
            need_tickle = scheduleNoLock(fc, thread);
        }
        if (need_tickle) {
            tickle();
        }
    }

    // 调度多个Fiber或回调函数，从迭代器范围内
    template <class InputIterator>
    void schedule(InputIterator begin, InputIterator end) {
        bool need_tickle = false;
        {
            MutexType::Lock lock(m_mutex);
            while (begin != end) {
                need_tickle = scheduleNoLock(&*begin, -1) || need_tickle;
                begin++;
            }
        }
        if (need_tickle) {
            tickle();
        }
    }

protected:
    // 主要运行函数，由线程执行
    void run();

    // 设置当前线程的调度器实例
    void setThis();

    // 触发调度事件
    virtual void tickle();
    // 空闲函数，在没有准备好运行的Fiber时执行
    virtual void idle();
    // 检查调度器是否正在停止
    virtual bool stopping();
    bool hasIdleThreads() { return m_idleThreadCount > 0; }

private:
    // 无锁调度Fiber或回调函数的私有方法
    template <class FiberOrCb>
    bool scheduleNoLock(FiberOrCb fb, int thread) {
        bool need_tickle = m_fibers.empty();
        FiberAndThread ft(fb, thread);
        if (ft.fiber || ft.cb) {
            m_fibers.push_back(ft);
        }
        return need_tickle;
    }

private:
    // 结构体，用于保存Fiber或回调函数及其关联的线程信息
    struct FiberAndThread {
        Fiber::ptr fiber;          // 指向Fiber对象的指针
        std::function<void()> cb;  // 回调函数
        int m_threads;             // 关联的线程ID

        // 不同类型Fiber或回调函数的构造函数
        FiberAndThread(Fiber::ptr f, int thr) : fiber(f), m_threads(thr) {}
        FiberAndThread(Fiber::ptr* f, int thr) : m_threads(thr) {
            fiber.swap(*f);
        }
        FiberAndThread(std::function<void()> f, int thr)
            : cb(f), m_threads(thr) {}
        FiberAndThread(std::function<void()>* f, int thr) : m_threads(thr) {
            cb.swap(*f);
        }

        // 默认构造函数
        FiberAndThread() : m_threads(-1) {}

        // 重置Fiber或回调函数
        void reset() {
            fiber = nullptr;
            cb = nullptr;
            m_threads = -1;
        }
    };

private:
    MutexType m_mutex;                   // 互斥锁，用于线程安全
    std::vector<Thread::ptr> m_threads;  // 调度器管理的线程vector
    std::list<FiberAndThread> m_fibers;  // 待调度的Fiber或回调函数列表
    std::string m_name;                  // 调度器名称
    Fiber::ptr m_rootFiber;              // 调度器关联的根Fiber

    // 额外的属性
    std::vector<int> m_threadIds;  // 调度器管理的线程ID列表
    size_t m_threadCount = 0;      // 线程数
    std::atomic<size_t> m_activeThreadCount = {0};  // 活动线程数
    std::atomic<size_t> m_idleThreadCount = {0};    // 空闲线程数
    bool m_stopping = true;   // 标志：调度器是否正在停止
    bool m_autoStop = false;  // 标志：是否自动停止调度器
    int m_rootThreads = 0;    // 根线程ID
};

}  // namespace sylar
