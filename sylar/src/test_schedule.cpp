#if 0
#include <unistd.h>

#include "fiber.h"
#include "log.h"
#include "scheduler.h"
#include "util.h"
static sylar::Logger::ptr g_logger = SYLAR_LOG_ROOT();
void test_fiber() {
    static int s_count = 5;
    SYLAR_LOG_INFO(g_logger) << "test in fiber s_count=" << s_count;
    sleep(1);
    if (--s_count >= 0) {
        sylar::Scheduler::GetThis()->schedule(&test_fiber);
    }
}
int main() {
    SYLAR_LOG_INFO(g_logger) << "main";
    sylar::Scheduler sc(3, false, "name");
    sc.start();
    sleep(2);
    SYLAR_LOG_INFO(g_logger) << "scheduler";
    sc.schedule(&test_fiber);
    sc.stop();
    SYLAR_LOG_INFO(g_logger) << "over";
}
#endif