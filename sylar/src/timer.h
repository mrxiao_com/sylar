#pragma once
#include <sys/types.h>

#include <cstddef>
#include <cstdint>
#include <functional>
#include <memory>
#include <set>
#include <vector>

#include "thread.h"
namespace sylar {
class TimerManager;
class Timer : public std::enable_shared_from_this<Timer> {
public:
    typedef std::shared_ptr<Timer> ptr;
    Timer(uint64_t ms, std::function<void()> cb, bool recurring,  // 循环定时器
          TimerManager* manager);
    Timer(uint64_t next);
    bool cancel();
    bool refresh();
    bool reset(uint64_t ms, bool from_now);

public:
    uint64_t m_next = 0;  // 精确的执行时间
    std::function<void()> m_cb;
    bool m_recurring = false;  // 是否循环定时器
    uint64_t m_ms = 0;         // 执行周期
    TimerManager* m_manger = nullptr;

public:
    struct Comparator {
        bool operator()(const Timer::ptr& lhs, const Timer::ptr& rhs) const;
    };
};

class TimerManager {
    friend class Timer;

public:
    typedef RWMutex RWMutexType;
    TimerManager();
    virtual ~TimerManager();

    Timer::ptr addTimer(uint64_t ms, std::function<void()> cb,
                        bool recurring = false);
    Timer::ptr addConditionTimer(uint64_t ms, std::function<void()> cb,
                                 std::weak_ptr<void> weak_cond,
                                 bool recurring = false);

    uint64_t getNextTimer();  // 获取下一个定时器的执行时间
    void listExpiredTimer(std::vector<std::function<void()>>& cbs);
    bool hasTimer();

protected:
    virtual void onTimerInsertedAtFront() = 0;
    void addTimer(Timer::ptr val, RWMutexType::WriteLock& lock);

private:
    bool detectClockRollover(uint64_t now_ms);

private:
    RWMutexType m_mutex;
    std::set<Timer::ptr, Timer::Comparator> m_timers;
    // 如果不提交比较函数比较的是地址
    bool m_tickled = false;
    uint64_t m_previousTime = 0;
};

}  // namespace sylar