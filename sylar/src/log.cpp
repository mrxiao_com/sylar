#include "log.h"

#include <bits/types/time_t.h>
#include <stdarg.h>
#include <yaml-cpp/node/node.h>

#include <cctype>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <functional>
#include <iostream>
#include <map>
#include <sstream>
#include <tuple>
#include <utility>

#include "config.h"
#include "thread.h"
namespace sylar {

std::string LogFormatter::format(std::shared_ptr<Logger> logger,
                                 LogLevel::Level level, LogEvent::ptr event) {
    std::stringstream ss;
    for (auto &i : m_items) {
        i->format(ss, logger, level, event);
    }
    return ss.str();
}

class MessageFormatItem : public LogFormatter::FormatItem {
public:
    MessageFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << LogLevel::ToString(level) << " " << event->getContent();
    }
};

class LevelFormatItem : public LogFormatter::FormatItem {
public:
    LevelFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << LogLevel::ToString(level);
        // out << LogLevel::ToString(level) << " " << event->getContent();
    }
};

class ElapseFormatItem : public LogFormatter::FormatItem {
public:
    ElapseFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << logger->getName();
    }
};

class ThreadIdFormatItem : public LogFormatter::FormatItem {
public:
    ThreadIdFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << event->getThreadId();
    }
};

class FiberIdFormatItem : public LogFormatter::FormatItem {
public:
    FiberIdFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << event->getFiberId();
    }
};

class ThreadNameFormatItem : public LogFormatter::FormatItem {
public:
    ThreadNameFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << event->getThreadName();
    }
};

class DataTimeFormatItem : public LogFormatter::FormatItem {
public:
    DataTimeFormatItem(const std::string &format = "%Y-%m-%d %H:%M:%S")
        : m_format(format) {
        if (m_format.empty()) {
            m_format = "%Y-%m-%d %H:%M:%S";
        }
    }

    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        struct tm tm;
        time_t rawtime = event->getTime();
        char buffer[80];
        // time(&rawtime);
        localtime_r(&rawtime, &tm);
        strftime(buffer, sizeof(buffer), m_format.c_str(), &tm);
        std::cout << buffer;
    }

private:
    std::string m_format;
};

class FileNameFormatItem : public LogFormatter::FormatItem {
public:
    FileNameFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << event->getFile();
    }
};
class NameFormatItem : public LogFormatter::FormatItem {
public:
    NameFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << event->getLogger()->getName();
    }
};

class LineFormatItem : public LogFormatter::FormatItem {
public:
    LineFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << event->getLine();
    }
};

class NewLineFormatItem : public LogFormatter::FormatItem {
public:
    NewLineFormatItem(const std::string &fmt = "") {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << std::endl;
    }
};

class StringFormatItem : public LogFormatter::FormatItem {
public:
    StringFormatItem(const std::string &str) : m_string(str) {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << m_string;
    }

private:
    std::string m_string;
};

class TabFormatItem : public LogFormatter::FormatItem {
public:
    TabFormatItem(const std::string &str) : m_string(str) {}
    virtual void format(std::ostream &out, std::shared_ptr<Logger> logger,
                        LogLevel::Level level, LogEvent::ptr event) override {
        out << "\t";
    }

private:
    std::string m_string;
};

LogEvent::~LogEvent() {}

LogEventWrap::LogEventWrap(LogEvent::ptr e) : m_event(e) {}
LogEventWrap::~LogEventWrap() {
    m_event->getLogger()->log(m_event->getLevel(), m_event);
}

void LogEvent::format(const char *fmt, ...) {
    va_list al;
    va_start(al, fmt);
    format(fmt, al);
    va_end(al);
}
void LogEvent::format(const char *fmt, va_list al) {
    char *buf = nullptr;
    int len = vasprintf(&buf, fmt, al);
    if (len != -1) {
        m_ss << std::string(buf, len);
        free(buf);
    }
}

std::stringstream &LogEventWrap::getSS() { return m_event->getSS(); }
// clang-format off
LogEvent::LogEvent(std::shared_ptr<Logger> logger, LogLevel::Level level,const char *file, int32_t line, uint32_t elapse,
                   uint32_t thread_id, uint32_t fiber_id, uint64_t time, const std::string &thread_name)
    : m_file(file), 
      m_line(line), 
      m_elapse(elapse), 
      m_threadId(thread_id),
      m_fiberId(fiber_id), 
      m_time(time),
      m_logger(logger),
      m_level(level),
      m_threadName(thread_name){}
// clang-format on
const char *LogLevel::ToString(LogLevel::Level level) {
    // clang-format off
    switch (level) {
#define XX(name) \
    case LogLevel::name:\
        return #name;
        break;
    XX(DEBUG);
    XX(INFO);
    XX(WARN);
    XX(ERROR);
    XX(FATAL);
#undef XX
    default:
        return "UNKNOWN";
    }
    // clang-format on
}

LogLevel::Level LogLevel::FromString(const std::string &str) {
#define XX(level, v)            \
    if (str == #v) {            \
        return LogLevel::level; \
    }
    XX(DEBUG, debug);
    XX(INFO, info);
    XX(WARN, warn);
    XX(ERROR, error);
    XX(FATAL, fatal);
    XX(DEBUG, DEBUG);
    XX(INFO, INFO);
    XX(WARN, WARN);
    XX(ERROR, ERROR);
    XX(FATAL, FATAL);
    return LogLevel::UNKNOW;
#undef XX
}

Logger::Logger(const std::string &name)
    : m_name(name), m_level(LogLevel::DEBUG) {
    m_formatter.reset(new LogFormatter(
        "%d{%Y-%m-%d %H:%M:%S}%T%t%T[%N]%T%F%T[%p]%T[%c]%T%f:%l%T%m%n"));
}
void Logger::log(LogLevel::Level level, const LogEvent::ptr event) {
    auto self = shared_from_this();
    MutexType::Lock lock(m_mutex);
    if (level >= m_level) {
        if (!m_appenders.empty())
            for (auto &i : m_appenders) {
                i->Log(self, level, event);
            }
        else if (m_root) {
            m_root->log(level, event);
        }
    }
}
void Logger::debug(LogEvent::ptr event) { log(LogLevel::DEBUG, event); }
void Logger::info(LogEvent::ptr event) { log(LogLevel::INFO, event); }
void Logger::warn(LogEvent::ptr event) { log(LogLevel::WARN, event); }
void Logger::error(LogEvent::ptr event) { log(LogLevel::ERROR, event); }
void Logger::fatal(LogEvent::ptr event) { log(LogLevel::FATAL, event); }
void Logger::addAppender(LogAppender::ptr appender) {
    MutexType::Lock lock(m_mutex);
    if (!appender->getFormatter()) {
        MutexType::Lock ll(appender->m_mutex);
        appender->m_formatter = m_formatter;
    }
    m_appenders.push_back(appender);
}
void Logger::delAppender(LogAppender::ptr appender) {
    MutexType::Lock lock(m_mutex);
    for (auto it = m_appenders.begin(); it != m_appenders.end(); it++) {
        if (*it == appender) {
            m_appenders.erase(it);
        }
    }
}

void Logger::clearAppenders() {
    MutexType::Lock lock(m_mutex);
    m_appenders.clear();
}

void Logger::setFormatter(LogFormatter::ptr val) {
    MutexType::Lock lock(m_mutex);
    m_formatter = val;
    for (auto &i : m_appenders) {
        MutexType::Lock ll(i->m_mutex);
        if (!i->m_hasFormatter) {
            i->m_formatter = m_formatter;
        }
    }
}
void Logger::setFormatter(const std::string &val) {
    sylar::LogFormatter::ptr new_val(new sylar::LogFormatter(val));
    if (new_val->isError()) {
        std::cout << "Logger setFormatter name = " << m_name << " value=" << val
                  << " invalid formatter" << std::endl;
        return;
    }
    // m_formatter = new_val;
    setFormatter(new_val);
}
LogFormatter::ptr Logger::getFormatter() {
    MutexType::Lock lock(m_mutex);
    return m_formatter;
}

FileLogAppender::FileLogAppender(const std::string &filename)
    : m_filename(filename) {
    reopen();
    if (!m_filestream.is_open()) {
        std::cout << "reopen " << filename << " failed:" << std::endl;
    }
}
void StdoutLogAppender::Log(std::shared_ptr<Logger> logger,
                            LogLevel::Level level, LogEvent::ptr event) {
    if (level >= m_level) {
        MutexType::Lock lock(m_mutex);
        std::cout << m_formatter->format(logger, level, event);
    }
}

std::string StdoutLogAppender::toYamlString() {
    MutexType::Lock lock(m_mutex);
    YAML::Node node;
    node["type"] = "StdoutLogAppender";
    if (m_level != LogLevel::UNKNOW) {
        node["level"] = LogLevel::ToString(m_level);
    }
    if (m_hasFormatter && m_formatter) {
        node["formatter"] = m_formatter->getPattern();
    }
    std::stringstream ss;
    ss << node;
    return ss.str();
}
void FileLogAppender::Log(std::shared_ptr<Logger> logger, LogLevel::Level level,
                          LogEvent::ptr event) {
    if (level >= m_level) {
        uint64_t now = time(0);
        if (now != m_lasttime) {
            reopen();
            m_lasttime = now;
        }
        MutexType::Lock lock(m_mutex);
        m_filestream << m_formatter->format(logger, level, event) << std::flush;
    }
}

std::string FileLogAppender::toYamlString() {
    MutexType::Lock lock(m_mutex);
    YAML::Node node;
    node["type"] = "FileLogAppender";
    node["file"] = m_filename;
    if (m_level != LogLevel::UNKNOW) {
        node["level"] = LogLevel::ToString(m_level);
    }
    if (m_hasFormatter && m_formatter) {
        node["formatter"] = m_formatter->getPattern();
    }
    std::stringstream ss;
    ss << node;
    return ss.str();
}
bool FileLogAppender::reopen() {
    MutexType::Lock lock(m_mutex);
    if (m_filestream) {
        m_filestream.close();
    }
    m_filestream.open(m_filename);
    return !!m_filestream;
}
LogFormatter::LogFormatter(const std::string &pattern) : m_pattern(pattern) {
    init();
}
LogFormatter::LogFormatter(std::ostream &out, const std::string &pattern)
    : m_pattern(pattern) {
    init();
}
void LogFormatter::init() {
    std::vector<std::tuple<std::string, std::string, int>> vec;
    std::string nstr;
    for (size_t i = 0; i < m_pattern.size(); ++i) {
        if (m_pattern[i] != '%') {
            nstr.append(1, m_pattern[i]);
            continue;
        }

        if ((i + 1) < m_pattern.size() && m_pattern[i + 1] == '%') {
            nstr.append(1, '%');
            ++i;  // 跳过第二个 '%'
            continue;
        }

        // 修改部分：提前处理 nstr
        if (!nstr.empty()) {
            vec.push_back(std::make_tuple(nstr, "", 0));
            nstr.clear();
        }

        size_t n = i + 1;
        int fmt_status = 0;
        size_t fmt_begin = 0;

        std::string str;
        std::string fmt;
        while (n < m_pattern.size()) {
            if (fmt_status == 0 && m_pattern[n] == '{') {
                str = m_pattern.substr(i + 1, n - i - 1);
                fmt_status = 1;  // 开始解析格式
                fmt_begin = n;
                ++n;
                continue;
            }
            if (fmt_status == 1 && m_pattern[n] == '}') {
                fmt = m_pattern.substr(fmt_begin + 1, n - fmt_begin - 1);
                fmt_status = 2;
                ++n;  // 跳到 '}' 后的下一个字符
                break;
            }
            // 关键的地方
            if (fmt_status == 0 && !isalpha(m_pattern[n])) {
                break;
            }
            ++n;
        }
        if (fmt_status == 0) {
            str = m_pattern.substr(i + 1, n - i - 1);
            vec.push_back(std::make_tuple(str, "", 1));
            i = n - 1;
        } else if (fmt_status == 1) {
            std::cout << "pattern parser error: " << m_pattern << " - "
                      << m_pattern.substr(i) << std::endl;
            m_error = true;
            vec.push_back(std::make_tuple("<<pattern error>>", "", 0));
        } else if (fmt_status == 2) {
            vec.push_back(std::make_tuple(str, fmt, 1));
            i = n - 1;
        }
    }
    if (!nstr.empty()) {
        vec.push_back(std::make_tuple(nstr, "", 0));
    }
    static std::map<std::string,
                    std::function<FormatItem::ptr(const std::string &str)>>
        s_format_items = {
    // clang-format off
#define XX(str, C)                                                             \
    {#str, [](const std::string &fmt) {return FormatItem::ptr(new C(fmt));}}
            XX(m, MessageFormatItem), 
            XX(p, LevelFormatItem),
            XX(r, ElapseFormatItem),  
            XX(c, NameFormatItem),
            XX(t, ThreadIdFormatItem),
            XX(n, NewLineFormatItem),
            XX(d, DataTimeFormatItem),
            XX(f, FileNameFormatItem),
            XX(l, LineFormatItem),
            XX(T, TabFormatItem),
            XX(F, FiberIdFormatItem),
            XX(N, ThreadNameFormatItem)
#undef XX
        };
    // clang-format on
    for (auto &i : vec) {
        if (std::get<2>(i) == 0) {
            m_items.push_back(
                FormatItem::ptr(new StringFormatItem(std::get<0>(i))));
        } else {
            auto it = s_format_items.find(std::get<0>(i));
            if (it == s_format_items.end()) {
                m_items.push_back(FormatItem::ptr(new StringFormatItem(
                    "<<error_format %" + std::get<0>(i) + ">>")));
                m_error = true;
            } else {
                m_items.push_back(it->second(std::get<1>(i)));
            }
        }
        // std::cout << "(" << std::get<0>(i) << ") - (" << std::get<1>(i)
        //           << ") - (" << std::get<2>(i) << ")" << std::endl;
    }
}  // namespace sylar

LoggerManager::LoggerManager() {
    m_root.reset(new Logger);
    m_root->addAppender(LogAppender::ptr(new StdoutLogAppender));
    m_loggers[m_root->m_name] = m_root;
    init();
}

std::string Logger::toYamlString() {
    YAML::Node node;
    node["name"] = m_name;
    if (m_level != LogLevel::UNKNOW) {
        node["level"] = LogLevel::ToString(m_level);
    }
    if (m_formatter) {
        node["formatter"] = m_formatter->getPattern();
    }
    for (auto &i : m_appenders) {
        node["appenders"].push_back(YAML::Load(i->toYamlString()));
    }
    std::stringstream ss;
    ss << node;
    return ss.str();
}

Logger::ptr LoggerManager::getLogger(const std::string &name) {
    MutexType::Lock lock(m_mutex);
    auto it = m_loggers.find(name);
    if (it != m_loggers.end()) {
        return it->second;
    }
    Logger::ptr logger(new Logger(name));
    logger->m_root = m_root;
    m_loggers[name] = logger;
    return logger;
}

std::string LoggerManager::toYamlString() {
    MutexType::Lock lock(m_mutex);
    YAML::Node node;
    for (auto &i : m_loggers) {
        node.push_back(YAML::Load(i.second->toYamlString()));
    }
    std::stringstream ss;
    ss << node;
    return ss.str();
}

void LogAppender::setFormatter(LogFormatter::ptr val) {
    MutexType::Lock lock(m_mutex);
    m_formatter = val;
    if (m_formatter) {
        m_hasFormatter = true;
    } else {
        m_hasFormatter = false;
    }
}

LogFormatter::ptr LogAppender::getFormatter() {
    MutexType::Lock lock(m_mutex);
    return m_formatter;
}

struct LogAppenderDefine {
    int type = 0;  // 1 File,2 Stdout
    LogLevel::Level level = LogLevel::UNKNOW;
    std::string formatter;
    std::string file;
    bool operator==(const LogAppenderDefine &oth) const {
        return type == oth.type && level == oth.level &&
               formatter == oth.formatter && file == oth.file;
    }
};
struct LogDefine {
    std::string name;
    LogLevel::Level level = LogLevel::Level::UNKNOW;
    std::string formatter;
    std::vector<LogAppenderDefine> appenders;
    bool operator==(const LogDefine &oth) const {
        return name == oth.name && level == oth.level &&
               formatter == oth.formatter && appenders == oth.appenders;
    }
    bool operator<(const LogDefine &oth) const { return name < oth.name; }
};

template <>
class LexicalCast<std::string, std::set<LogDefine>> {
public:
    std::set<LogDefine> operator()(const std::string &v) {
        YAML::Node node = YAML::Load(v);
        std::set<LogDefine> s;
        // std::string document;
        for (std::size_t i = 0; i < node.size(); ++i) {
            auto n = node[i];
            if (!n["name"].IsDefined()) {
                std::cout << "log config error: name is null" << n << std::endl;
                continue;
            }
            LogDefine ld;
            ld.name = n["name"].as<std::string>();
            ld.level = LogLevel::FromString(
                n["level"].IsDefined() ? n["level"].as<std::string>() : "");
            if (n["formatter"].IsDefined()) {
                ld.formatter = n["formatter"].as<std::string>();
            }
            if (n["appenders"].IsDefined()) {
                for (size_t x = 0; x < n["appenders"].size(); ++x) {
                    auto a = n["appenders"][x];
                    if (!a["type"].IsDefined()) {
                        std::cout << "log config error: appender type is null"
                                  << a << std::endl;
                        continue;
                    }
                    std::string type = a["type"].as<std::string>();
                    LogAppenderDefine lad;
                    if (type == "FileLogAppender") {
                        lad.type = 1;
                        if (!a["file"].IsDefined()) {
                            std::cout << "log config error: fileappender file "
                                         "is null,"
                                      << a << std::endl;
                            continue;
                        }
                        lad.file = a["file"].as<std::string>();
                        if (a["formatter"].IsDefined()) {
                            lad.formatter = a["formatter"].as<std::string>();
                        }
                    } else if (type == "StdoutLogAppender") {
                        lad.type = 2;
                    } else {
                        std::cout
                            << "log config error: appender type is invalid,"
                            << a << std::endl;
                        continue;
                    }
                    ld.appenders.push_back(lad);
                }
            }
            std::cout << "----" << ld.name << "-" << ld.appenders.size()
                      << std::endl;
            s.insert(ld);
        }
        return s;
    }
};

template <>
class LexicalCast<std::set<LogDefine>, std::string> {
public:
    std::string operator()(const std::set<LogDefine> &v) {
        YAML::Node node;
        for (auto &i : v) {
            YAML::Node n;
            n["name"] = i.name;
            if (i.level != LogLevel::UNKNOW) {
                n["level"] = LogLevel::ToString(i.level);
            }
            if (i.formatter.empty()) {
                n["formatter"] = i.formatter;
            }
            for (auto &a : i.appenders) {
                YAML::Node na;
                if (a.type == 1) {
                    na["type"] = "FileLogAppender";
                    na["file"] = a.file;
                } else if (a.type == 2) {
                    na["type"] = "StdoutLogAppender";
                }
                if (a.level != LogLevel::UNKNOW) {
                    na["level"] = LogLevel::ToString(a.level);
                }
                if (!a.formatter.empty()) {
                    na["formatter"] = a.formatter;
                }
                n["appenders"].push_back(na);
            }
            node.push_back(n);
        }
        std::stringstream ss;
        ss << node;
        return ss.str();
    }
};

sylar::ConfigVar<std::set<LogDefine>>::ptr g_log_defines =
    sylar::Config::Lookup("logs", std::set<LogDefine>(), "logs config");

struct LogIniter {
    LogIniter() {
        g_log_defines->addListener(
            0xF1E231, [](const std::set<LogDefine> &old_value,
                         const std::set<LogDefine> &new_value) {
                // SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "on_logger_conf_changed";
                // 新增
                for (auto &i : new_value) {
                    auto it = old_value.find(i);
                    sylar::Logger::ptr logger;
                    if (it == old_value.end()) {
                        // 新增Logger
                        logger = SYLAR_LOG_NAME(i.name);
                    } else {
                        if (!(i == *it)) {
                            // 修改的Logger
                            logger = SYLAR_LOG_NAME(i.name);
                        }
                    }
                    logger->setLevel(i.level);
                    if (!i.formatter.empty()) {
                        logger->setFormatter(i.formatter);
                    }
                    logger->clearAppenders();
                    for (auto &a : i.appenders) {
                        sylar::LogAppender::ptr ap;
                        if (a.type == 1) {
                            ap.reset(new FileLogAppender(a.file));
                        } else if (a.type == 2) {
                            ap.reset(new StdoutLogAppender);
                        }
                        if (a.level == LogLevel::UNKNOW) {
                            ap->setLevel(i.level);
                        } else {
                            ap->setLevel(a.level);
                        }
                        if (!a.formatter.empty()) {
                            LogFormatter::ptr fmt(
                                new LogFormatter(a.formatter));
                            if (!fmt->isError()) {
                                ap->setFormatter(fmt);
                            } else {
                                std::cout << "log name=" << i.name
                                          << "appender type=" << a.type
                                          << " formatter=" << a.formatter
                                          << " is invlid" << std::endl;
                            }
                        }
                        logger->addAppender(ap);
                    }
                }
                // 删除
                for (auto &i : old_value) {
                    auto it = new_value.find(i);
                    if (it == new_value.end()) {
                        // 删除logger
                        auto logger = SYLAR_LOG_NAME(i.name);
                        logger->setLevel((LogLevel::Level)100);
                        logger->clearAppenders();
                    }
                }
            });
    }
};

static LogIniter __log_init;

void LoggerManager::init() {}

};  // namespace sylar

// m_items = vec;
//%m -- 消息体
//%p -- level
//%r -- 启动后的时间
//%c -- 日志名称
//%t -- 线程id
//%n -- 回车换行
//%d -- 时间
//%f -- 文件名
//%l -- 行号
