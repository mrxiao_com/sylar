#if 0
#include <assert.h>

#include "log.h"
#include "macro.h"
#include "util.h"
sylar::Logger::ptr g_logger = SYLAR_LOG_ROOT();
void test_assert() {
    // SYLAR_LOG_INFO(g_logger) << sylar::BackstraceToString(10);
    SYLAR_ASSERT(false);
}
void func1() { test_assert(); }
void func2() { func1(); }
void func3() { func2(); }
void func4() { func3(); }
void func5() { func4(); }
int main() { func5(); }
#endif