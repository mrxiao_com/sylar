#include "thread.h"

#include <pthread.h>
#include <semaphore.h>

#include <stdexcept>

#include "log.h"
#include "util.h"

namespace sylar {
inline static sylar::Logger::ptr g_logger = SYLAR_LOG_NAME("system");

Semaphore::Semaphore(uint32_t count) {
    if (sem_init(&m_semaphore, 0, count)) {
        throw std::logic_error("sem_init failed");
    }
}
Semaphore::~Semaphore() { sem_destroy(&m_semaphore); }
void Semaphore::wait() {
    while (true) {
        if (!sem_wait(&m_semaphore)) {
            return;
        }
    }
}
void Semaphore::notify() {
    if (sem_post(&m_semaphore)) {
        throw std::logic_error("sem_post error");
    }
}

Thread::Thread(std::function<void()> cb, const std::string& name)
    : m_cb(std::move(cb)), m_name(name) {
    if (name.empty()) {
        m_name = "UNKNOW";
    }
    int rt = pthread_create(&m_thread, nullptr, run, this);
    if (rt) {
        SYLAR_LOG_ERROR(g_logger) << "pthread_create thread failed, rt = " << rt
                                  << " name = " << name;
        throw std::logic_error("pthread_create error");
    }
    m_semaphore.wait();
}
Thread::~Thread() {
    if (m_thread) {
        // pthread_detach(m_thread);
        // join();
    }
}
void Thread::join() {
    if (m_thread) {
        int rt = pthread_join(m_thread, nullptr);
        if (rt) {
            SYLAR_LOG_ERROR(g_logger) << "join failed" << std::endl;
        }
    }
}

Thread* Thread::GetThis() { return t_thread; }
const std::string& Thread::GetName() {
    //
    return t_thread_name;
}
void* Thread::run(void* arg) {
    Thread* thread = (Thread*)arg;
    t_thread = thread;
    thread->m_id = sylar::GetThreadId();
    pthread_setname_np(pthread_self(), thread->m_name.substr(0, 15).c_str());
    SetName(thread->m_name);
    std::function<void()> cb;
    cb.swap(thread->m_cb);
    thread->m_semaphore.notify();
    cb();
    return 0;
}
void Thread::SetName(const std::string& name) {
    if (t_thread) {
        t_thread->m_name = name;
    }
    t_thread_name = name;
}
}  // namespace sylar