#include "config.h"

#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>
#include <yaml-cpp/yaml.h>

#include <utility>

#include "log.h"
#if 0

sylar::ConfigVar<int>::ptr g_int_value_config =
    sylar::Config::Lookup("system.port", (int)8080, "system port");
sylar::ConfigVar<float>::ptr g_int_valuex_config =
    sylar::Config::Lookup("system.port", (float)8080, "system port");
sylar::ConfigVar<float>::ptr g_int_float_config =
    sylar::Config::Lookup("system.value", (float)10.4f, "system value");
sylar::ConfigVar<std::vector<int>>::ptr g_int_vec_value_config =
    sylar::Config::Lookup("system.int_vec", std::vector<int>{1, 2},
                          "system int vec");
sylar::ConfigVar<std::list<int>>::ptr g_int_list_value_config =
    sylar::Config::Lookup("system.int_list", std::list<int>{3, 4, 5},
                          "system int list");
sylar::ConfigVar<std::set<int>>::ptr g_int_set_value_config =
    sylar::Config::Lookup("system.int_set", std::set<int>{6, 7, 8},
                          "system int set");
sylar::ConfigVar<std::unordered_set<int>>::ptr g_int_uset_value_config =
    sylar::Config::Lookup("system.int_uset", std::unordered_set<int>{9, 10, 11},
                          "system int unordered_set");
sylar::ConfigVar<std::map<std::string, int>>::ptr g_str_int_map_value_config =
    sylar::Config::Lookup("system.str_int_map",
                          std::map<std::string, int>{{"k", 2}},
                          "system str int map");
sylar::ConfigVar<std::unordered_map<std::string, int>>::ptr
    g_str_int_umap_value_config = sylar::Config::Lookup(
        "system.str_int_umap", std::unordered_map<std::string, int>{{"k", 2}},
        "system str int umap");

void print_yaml(const YAML::Node& node, int level) {
    if (node.IsScalar()) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
            << std::string(level * 4, ' ') << node.Scalar() << " - "
            << node.Type() << " - " << level;
    } else if (node.IsNull()) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
            << std::string(level * 4, ' ') << "NULL - " << node.Type() << " - "
            << level;
    } else if (node.IsMap()) {
        for (auto it = node.begin(); it != node.end(); ++it) {
            SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
                << std::string(level * 4, ' ') << it->first << " - "
                << it->second.Type() << " - " << level;
            print_yaml(it->second, level + 1);
        }
    } else if (node.IsSequence()) {
        for (size_t i = 0; i < node.size(); ++i) {
            SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
                << i << " - " << node[i].Type() << " - " << level;
            print_yaml(node[i], level + 1);
        }
    }
}
void test_yaml() {
    YAML::Node root = YAML::LoadFile("./log.yaml");
    SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << root;
    print_yaml(root, 0);
}
void test_config() {
    SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
        << "before: " << g_int_value_config->getValue();
    SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
        << "before: " << g_int_float_config->toString();
#define XX(g_var, name, prefix)                                              \
    {                                                                        \
        auto& v = g_var->getValue();                                         \
        for (auto& i : v) {                                                  \
            SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << #prefix " " #name ": " << i; \
        }                                                                    \
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT())                                     \
            << #prefix " " #name " yaml: " << g_var->toString();             \
    }

#define XX_M(g_var, name, prefix)                                          \
    {                                                                      \
        auto& v = g_var->getValue();                                       \
        for (auto& i : v) {                                                \
            SYLAR_LOG_INFO(SYLAR_LOG_ROOT())                               \
                << #prefix " " #name ": (" << i.first << " - " << i.second \
                << "}";                                                    \
        }                                                                  \
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT())                                   \
            << #prefix " " #name " yaml: " << g_var->toString();           \
    }
    XX(g_int_vec_value_config, int_vec, before);
    XX(g_int_list_value_config, int_list, before);
    XX(g_int_set_value_config, int_set, before);
    XX(g_int_uset_value_config, int_uset, before);
    XX_M(g_str_int_map_value_config, str_int_map, before);
    XX_M(g_str_int_umap_value_config, str_int_umap, before);

    YAML::Node root = YAML::LoadFile("./log.yaml");
    sylar::Config::LoadFromYaml(root);
    auto& v = g_int_vec_value_config->getValue();
    for (auto& i : v) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "int_vec: " << i;
    }
    auto& l = g_int_list_value_config->getValue();
    for (auto& i : l) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "int_list: " << i;
    }
    auto& s = g_int_set_value_config->getValue();
    for (auto& i : s) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "int_set: " << i;
    }
    auto& us = g_int_uset_value_config->getValue();
    for (auto& i : us) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "int_uset: " << i;
    }
    auto& mp = g_str_int_map_value_config->getValue();
    for (auto& i : us) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "int_map: " << i;
    }
    auto& ump = g_str_int_map_value_config->getValue();
    for (auto& i : us) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "int_umap: " << i;
    }
    XX(g_int_vec_value_config, int_vect, after);
    XX(g_int_list_value_config, int_list, after);
    XX(g_int_set_value_config, int_set, after);
    XX(g_int_uset_value_config, int_uset, after);
    XX_M(g_str_int_map_value_config, str_int_map, after);
    XX_M(g_str_int_umap_value_config, str_int_umap, after);

    sylar::Config::Visit([](sylar::ConfigVarbase::ptr var) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
            << var->getName() << " description= " << var->getDescription()
            << " typename=" << var->getTypeName()
            << " value=" << var->toString();
    });
#undef XX
#undef XX_M
}
class Person {
public:
    std::string m_name = {};
    int m_age = 0;
    bool m_sex = false;
    std::string toString() const {
        std::stringstream ss;
        ss << "[Person name=" << m_name << " age=" << m_age << " sex=" << m_sex
           << "]";
        return ss.str();
    }
    Person() = default;
    Person(const std::string& name, int age, bool sex)
        : m_name(name), m_age(age), m_sex(sex) {}
    bool operator==(const Person& oth) const {
        return m_name == oth.m_name && m_age == oth.m_age && m_sex == oth.m_sex;
    }
};

namespace sylar {
template <>
class LexicalCast<std::string, Person> {
public:
    Person operator()(const std::string& v) {
        YAML::Node node = YAML::Load(v);
        Person per;
        if (node.IsMap()) {
            per.m_name = node["name"].as<std::string>();
            per.m_age = node["age"].as<int>();
            per.m_sex = node["sex"].as<bool>();
        } else {
            // Handle error: node is not a map as expected
            throw std::runtime_error(
                "Invalid YAML format: expected a map structure");
        }
        return per;
    }
};

template <>
class LexicalCast<Person, std::string> {
public:
    std::string operator()(const Person& per) {
        YAML::Node node;
        node["name"] = per.m_name;
        node["age"] = per.m_age;
        node["sex"] = per.m_sex;
        std::stringstream ss;
        ss << node;
        return ss.str();
    }
};
}  // namespace sylar

sylar::ConfigVar<Person>::ptr g_person =
    sylar::Config::Lookup("class.person", Person{}, "system person");

sylar::ConfigVar<std::map<std::string, Person>>::ptr g_person_map =
    sylar::Config::Lookup(
        "class.map",
        std::map<std::string, Person>({{"test", {"name", 0, false}}}),
        "system person");
sylar::ConfigVar<std::map<std::string, std::vector<Person>>>::ptr
    g_person_vec_map = sylar::Config::Lookup(
        "class.vec_map",
        std::map<std::string, std::vector<Person>>(
            {{"test", {{"name", 0, false}, {"name2", 0, false}}}}),
        "system person");
void test_class() {
    SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
        << g_person->getValue().toString() << " - " << g_person->toString();
#define XX_PM(g_var, prefix)                                                 \
    {                                                                        \
        auto m = g_person_map->getValue();                                   \
        for (auto& i : m) {                                                  \
            SYLAR_LOG_INFO(SYLAR_LOG_ROOT())                                 \
                << prefix << ":" << i.first << " - " << i.second.toString(); \
        }                                                                    \
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << prefix << ":size=" << m.size();  \
    }

#define XX_VEC_PM(g_var, prefix)                                            \
    {                                                                       \
        auto m = g_person_vec_map->getValue();                              \
        for (auto& [key, persons] : m) {                                    \
            for (auto& person : persons) {                                  \
                SYLAR_LOG_INFO(SYLAR_LOG_ROOT())                            \
                    << prefix << ":" << key << person.toString();           \
            }                                                               \
        }                                                                   \
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << prefix << ":size=" << m.size(); \
    }

    g_person->addListener(10,
                          [](const Person& old_value, const Person& new_value) {
                              SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
                                  << "---------------------------"
                                  << "old_value=" << old_value.toString() << " "
                                  << "new_value=" << new_value.toString();
                          });
    XX_PM(g_person_map, "class.map before");
    XX_VEC_PM(g_person_vec_map, "class.vec_map before");

    SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
        << "before: " << g_person_vec_map->toString();

    YAML::Node root = YAML::LoadFile("./log.yaml");
    sylar::Config::LoadFromYaml(root);
    SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
        << "after:" << g_person->getValue().toString() << " - "
        << g_person->toString();
    SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "after:" << g_person_vec_map->toString()
                                     << " - " << g_person->toString();
    XX_PM(g_person_map, "class.map after");
    XX_VEC_PM(g_person_vec_map, "class.vec_map after");
};
void test_log() {
    static sylar::Logger::ptr system_log = SYLAR_LOG_NAME("system");
    SYLAR_LOG_INFO(system_log) << "hello system" << std::endl;
    // std::cout << sylar::LoggerMgr::GetInstance()->toYamlString() <<
    // std::endl;
    YAML::Node root = YAML::LoadFile("./log.yaml");
    sylar::Config::LoadFromYaml(root);
    std::cout << "============================================" << std::endl;
    std::cout << sylar::LoggerMgr::GetInstance()->toYamlString() << std::endl;
    std::cout << root << std::endl;
    SYLAR_LOG_INFO(system_log) << "hello system end" << std::endl;
    system_log->setFormatter("%d - %m%n");
    SYLAR_LOG_INFO(system_log) << "hello system end" << std::endl;
}
int main() {
    // test_yaml()
    test_config();
    // test_class();
    // test_log();
}
#endif
