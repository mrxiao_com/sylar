
#include "scheduler.h"

#include <cstddef>
#include <functional>
#include <string>

#include "assert.h"
#include "fiber.h"
#include "hook.h"
#include "log.h"
#include "macro.h"
#include "thread.h"
#include "util.h"

namespace sylar {

static sylar::Logger::ptr g_logger = SYLAR_LOG_NAME("system");
static thread_local Scheduler* t_scheduler = nullptr;
static thread_local Fiber* t_fiber = nullptr;

// 构造函数
Scheduler::Scheduler(size_t threads, bool use_caller, const std::string& name)
    : m_name(name) {
    SYLAR_ASSERT(threads > 0);

    // 如果使用调用者的Fiber，初始化根Fiber
    if (use_caller) {
        sylar::Fiber::GetThis();
        --threads;
        SYLAR_ASSERT(GetThis() == nullptr);
        t_scheduler = this;
        m_rootFiber.reset(new Fiber(std::bind(&Scheduler::run, this), 0, true));
        sylar::Thread::SetName(m_name);
        t_fiber = m_rootFiber.get();
        m_rootThreads = sylar::GetThreadId();
        m_threadIds.push_back(m_rootThreads);
    } else {
        m_rootThreads = -1;
    }
    m_threadCount = threads;
}

// 析构函数
Scheduler::~Scheduler() {
    SYLAR_ASSERT(m_stopping);
    if (GetThis() == this) {
        t_scheduler = nullptr;
    }
}

// 静态方法：获取当前线程关联的调度器
Scheduler* Scheduler::GetThis() { return t_scheduler; }

// 静态方法：获取当前线程关联的主要Fiber
Fiber* Scheduler::GetMainFiber() { return t_fiber; }

// 启动调度器
void Scheduler::start() {
    MutexType::Lock lock(m_mutex);
    if (!m_stopping) {
        return;
    }
    m_stopping = false;
    SYLAR_ASSERT(m_threads.empty());

    // 创建并初始化线程
    m_threads.resize(m_threadCount);
    for (size_t i = 0; i < m_threadCount; ++i) {
        m_threads[i].reset(new Thread(std::bind(&Scheduler::run, this),
                                      m_name + "_" + std::to_string(i)));
        m_threadIds.push_back(m_threads[i]->getId());
    }
    lock.unlock();

    // 如果存在根Fiber，则启动它
    // if (m_rootFiber) {
    //     // m_rootFiber->swapIn();
    //     m_rootFiber->call();
    //     SYLAR_LOG_INFO(g_logger) << "call out" << m_rootFiber->getState();
    // }
}

// 停止调度器
void Scheduler::stop() {
    m_autoStop = true;
    if (m_rootFiber && m_threadCount == 0 &&
        ((m_rootFiber->getState() == Fiber::TERM) ||
         (m_rootFiber->getState() == Fiber::INIT))) {
        SYLAR_LOG_INFO(g_logger) << this << " stopped";
        m_stopping = true;
        if (stopping()) {
            return;
        }
    }
    // bool exit_on_this_fiber = false;
    if (m_rootThreads != -1) {
        SYLAR_ASSERT(GetThis() == this);
    } else {
        SYLAR_ASSERT(GetThis() != this);
    }
    m_stopping = true;

    // 唤醒所有线程以处理停止事件
    for (size_t i = 0; i < m_threadCount; ++i) {
        tickle();
    }
    if (m_rootFiber) {
        tickle();
    }
    if (stopping()) {
        return;
    }
    if (m_rootFiber) {
        // while (!stopping()) {
        // if (m_rootFiber->getState() == Fiber::TERM ||
        //     m_rootFiber->getState() == Fiber::EXCEP) {
        //     m_rootFiber.reset(
        //         new Fiber(std::bind(&Scheduler::run, this), 0, true));
        //     SYLAR_LOG_INFO(g_logger) << "root fiber is term, reset";
        //     t_fiber = m_rootFiber.get();
        // }
        // m_rootFiber->call();
        // }
        if (!stopping()) {
            m_rootFiber->call();
        }
    }
    std::vector<Thread::ptr> thrs;
    {
        MutexType::Lock lock(m_mutex);
        thrs.swap(m_threads);
    }
    for (auto& i : thrs) {
        i->join();
    }
    // if (exit_on_this_fiber) {
    // }
}

// 设置当前线程的调度器实例
void Scheduler::setThis() { t_scheduler = this; }

// 主要运行函数，由线程执行
void Scheduler::run() {
    SYLAR_LOG_INFO(g_logger) << "run";
    set_hook_enable(true);
    setThis();
    if (sylar::GetThreadId() != m_rootThreads) {
        t_fiber = Fiber::GetThis().get();
    }
    Fiber::ptr idle_fiber(new Fiber(std::bind(&Scheduler::idle, this)));
    Fiber::ptr cb_fiber;

    FiberAndThread ft;
    while (true) {
        ft.reset();
        bool tickle_me = false;
        bool is_active = false;
        {
            MutexType::Lock lock(m_mutex);

            // 遍历Fiber列表，寻找可执行的Fiber或回调函数
            auto it = m_fibers.begin();
            while (it != m_fibers.end()) {
                // 检查是否指定了线程ID，并且不是当前线程
                if (it->m_threads != -1 &&
                    it->m_threads != sylar::GetThreadId()) {
                    ++it;
                    tickle_me = true;
                    continue;
                }

                // 断言：Fiber或回调函数不能为空
                SYLAR_ASSERT(it->fiber || it->cb);

                // 如果是Fiber并且状态为执行中，则继续下一个
                if (it->fiber && it->fiber->getState() == Fiber::EXEC) {
                    ++it;
                    continue;
                }

                // 找到可执行的Fiber或回调函数
                ft = *it;
                m_fibers.erase(it);
                ++m_activeThreadCount;
                is_active = true;
                break;
            }
        }

        // 如果有需要唤醒其他线程的标志，则执行唤醒
        if (tickle_me) {
            tickle();
        }

        // 执行找到的Fiber或回调函数
        if (ft.fiber && ft.fiber->getState() != Fiber::TERM) {
            ft.fiber->swapIn();
            --m_activeThreadCount;
            if (ft.fiber->getState() == Fiber::READY) {
                schedule(ft.fiber);
            } else if (ft.fiber->getState() != Fiber::TERM &&
                       ft.fiber->getState() != Fiber::EXCEP) {
                ft.fiber->m_state = Fiber::HOLD;
            }
            ft.reset();
        } else if (ft.cb) {
            if (cb_fiber) {
                cb_fiber->reset(ft.cb);
            } else {
                cb_fiber.reset(new Fiber(ft.cb));
            }
            ft.reset();
            cb_fiber->swapIn();
            --m_activeThreadCount;
            if (cb_fiber->getState() == Fiber::READY) {
                schedule(cb_fiber);
                cb_fiber.reset();
            } else if (cb_fiber->getState() == Fiber::EXCEP ||
                       cb_fiber->getState() == Fiber::TERM) {
                cb_fiber->reset(nullptr);
            } else {  // if (cb_fiber->getState() != Fiber::TERM) {
                cb_fiber->m_state = Fiber::HOLD;
                cb_fiber.reset();
            }
        } else {
            if (is_active) {
                --m_activeThreadCount;
                continue;
            }

            // 如果空闲Fiber已经结束，退出循环
            if (idle_fiber->getState() == Fiber::TERM) {
                SYLAR_LOG_INFO(g_logger) << "idle fiber term";
                break;
            }
            ++m_idleThreadCount;
            idle_fiber->swapIn();
            if (idle_fiber->getState() != Fiber::TERM &&
                idle_fiber->getState() != Fiber::EXCEP) {
                idle_fiber->m_state = Fiber::HOLD;
            }
            --m_idleThreadCount;
        }
    }
}

// 触发调度事件，唤醒所有线程
void Scheduler::tickle() { SYLAR_LOG_INFO(g_logger) << "tickle"; }

// 检查调度器是否正在停止
bool Scheduler::stopping() {
    // SYLAR_LOG_INFO(g_logger) << "stopping";
    MutexType::Lock lock(m_mutex);
    return m_autoStop && m_stopping && m_fibers.empty() &&
           (m_activeThreadCount == 0);
}

// 空闲函数，在没有准备好运行的Fiber时执行
void Scheduler::idle() {
    SYLAR_LOG_INFO(g_logger) << "idle";
    while (!stopping()) {
        sylar::Fiber::YieldToHold();
    }
}

}  // namespace sylar