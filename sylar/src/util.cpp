#include "util.h"

#include <bits/types/struct_timeval.h>
#include <cxxabi.h>
#include <dlfcn.h>
#include <execinfo.h>
#include <sys/select.h>
#include <sys/syscall.h>
#include <unistd.h>

#include <cstring>
#include <iostream>
#include <sstream>
#include <vector>

#include "fiber.h"
#include "log.h"
namespace sylar {
sylar::Logger::ptr g_logger = SYLAR_LOG_NAME("system");
pid_t GetThreadId() { return syscall(SYS_gettid); }
uint32_t GetFiberId() { return sylar::Fiber::GetFiberId(); }
void Backtrace(std::vector<std::string>& bt, int size, int skip) {
    void** array = (void**)malloc(sizeof(void*) * size);
    size_t s = ::backtrace(array, size);

    char** strings = backtrace_symbols(array, s);
    if (strings == NULL) {
        SYLAR_LOG_ERROR(g_logger) << "backtrace_symbols error";
        free(array);
        return;
    }

    for (size_t i = skip; i < s; ++i) {
        Dl_info info;
        if (dladdr(array[i], &info) && info.dli_sname) {
            int status;
            char* demangled =
                abi::__cxa_demangle(info.dli_sname, 0, 0, &status);
            std::stringstream ss;
            ss << info.dli_fname << "(";
            if (status == 0) {
                ss << demangled;
            } else {
                ss << info.dli_sname;
            }
            ss << ") [" << array[i] << "]";
            bt.push_back(ss.str());
            free(demangled);
        } else {
            std::stringstream ss;
            ss << strings[i] << " [" << array[i] << "]";
            bt.push_back(ss.str());
        }
    }

    free(strings);
    free(array);
}

std::string BackstraceToString(int size, int skip, const std::string& prefix) {
    std::vector<std::string> bt;
    Backtrace(bt, size, skip);
    std::stringstream ss;
    for (size_t i = 0; i < bt.size(); ++i) {
        if (!bt[i].empty()) {  // Check if the line is not empty
            ss << prefix << bt[i] << std::endl;
        }
    }
    return ss.str();
}

uint64_t GetCurrentMS() {
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    return tv.tv_sec * 1000ul + tv.tv_usec / 1000;
}
uint64_t GetCurrentUS() {
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    return tv.tv_sec * 1000 * 1000ul + tv.tv_usec;
}
}  // namespace sylar