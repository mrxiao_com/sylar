#if 0
#include <string>

#include "fiber.h"
#include "log.h"
#include "thread.h"
#include "util.h"

static sylar::Logger::ptr g_logger = SYLAR_LOG_ROOT();

void run_in_fiber() {
    SYLAR_LOG_INFO(g_logger) << "run_in_fiber begin";
    sylar::Fiber::YieldToRead();
    SYLAR_LOG_INFO(g_logger) << "run_in_fiber end";
    sylar::Fiber::YieldToHold();
}

void test_fiber() {
    SYLAR_LOG_INFO(g_logger) << "main begin -1";
    {
        sylar::Fiber::GetThis();  // 初始化主协程
        SYLAR_LOG_INFO(g_logger) << "main begin";
        sylar::Fiber::ptr fiber(new sylar::Fiber(run_in_fiber));
        fiber->swapIn();  // 切换到子协程执行
        SYLAR_LOG_INFO(g_logger) << "main after swapIn";
        fiber->swapIn();  // 再次切换到子协程执行
        SYLAR_LOG_INFO(g_logger) << "main after end1";
        fiber->swapIn();  // 最后一次切换到子协程执行
    }
    SYLAR_LOG_INFO(g_logger) << "main after end2";
}

int main() {
    sylar::Thread::SetName("main");
    std::vector<sylar::Thread::ptr> thrs;
    for (int i = 0; i < 3; i++) {
        thrs.push_back(sylar::Thread::ptr(new sylar::Thread(
            &test_fiber, std::string("name_") + std::to_string(i))));
    }
    for (auto i : thrs) {
        i->join();
    }
    return 0;
}
#endif