#if 0
#include "thread.h"

#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>
#include <yaml-cpp/yaml.h>

#include <string>

#include "config.h"
#include "log.h"
#include "util.h"
sylar::Logger::ptr g_logger = SYLAR_LOG_ROOT();
int count = 0;
// sylar::RWMutex s_mutex;
sylar::Mutex mutex;
void func1() {
    SYLAR_LOG_INFO(g_logger)
        << "name:" << sylar::Thread::GetName()
        << " this.name:" << sylar::Thread::GetThis()->getName()
        << " id: " << sylar::GetThreadId()
        << " this.id: " << sylar::Thread::GetThis()->getId();
    for (int i = 0; i < 100000; i++) {
        // sylar::RWMutex::WriteLock lock(s_mutex);
        sylar::Mutex::Lock lock(mutex);
        count++;
    }
}
void func2() {
    while (true) {
        SYLAR_LOG_INFO(g_logger) << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}
void func3() {
    while (true) {
        SYLAR_LOG_INFO(g_logger) << "=================================";
    }
}
int main(int argc, char *argv[]) {
    SYLAR_LOG_INFO(g_logger) << "thread test begin";
    YAML::Node root = YAML::LoadFile("./log2.yaml");
    sylar::Config::LoadFromYaml(root);
    std::vector<sylar::Thread::ptr> thrs;
    for (int i = 0; i < 5; ++i) {
        sylar::Thread::ptr thr(
            new sylar::Thread(func2, "name_" + std::to_string(i * 2)));
        sylar::Thread::ptr thr2(
            new sylar::Thread(func3, "name_" + std::to_string(i * 2 + 1)));
        thrs.push_back(thr);
        thrs.push_back(thr2);
    }
    for (int i = 0; i < thrs.size(); ++i) {
        thrs[i]->join();
    }
    SYLAR_LOG_INFO(g_logger) << "thread test end";
    SYLAR_LOG_INFO(g_logger) << "count=" << count;
    return 0;
}

#endif