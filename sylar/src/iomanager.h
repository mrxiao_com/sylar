#pragma once
#include <atomic>
#include <functional>
#include <memory>
#include <string>

#include "fiber.h"
#include "scheduler.h"
#include "thread.h"
#include "timer.h"
namespace sylar {
class IOManager : public Scheduler, public TimerManager {
public:
    typedef std::shared_ptr<IOManager> ptr;
    typedef RWMutex RWMutexType;
    enum Event {
        NONE = 0x0,
        READ = 0x1,  // EPOLLIN
        WRITE = 0x4  // EPOLLOUT
    };

private:
    struct FdContext {
        typedef Mutex MutexType;
        struct EventContext {
            Scheduler* scheduler = nullptr;  // 事件执行的scheduler
            Fiber::ptr fiber;                // 事件的协程
            std::function<void()> cb;        // 事件的回调函数
        };
        int fd = 0;  // 事件相关的句柄
        EventContext& getContext(Event event);
        void resetContext(EventContext& ctx);
        void triggerEvent(Event event);
        EventContext read;    // 读事件
        EventContext write;   // 写事件
        Event events = NONE;  // 已经注册的事件
        MutexType mutex;
    };

public:
    IOManager(size_t threads = 1, bool use_caller = true,
              const std::string& name = "");
    ~IOManager();
    // 1. success 0 retry -1 error
    int addEvent(int fd, Event event, std::function<void()> cb = nullptr);
    bool delEvent(int fd, Event event);
    bool cancelEvent(int fd, Event event);
    bool cancelAll(int fd);
    static IOManager* GetThis();
    // 触发调度事件
    virtual void tickle() override;
    // 空闲函数，在没有准备好运行的Fiber时执行
    virtual void idle() override;
    // 检查调度器是否正在停止
    virtual bool stopping() override;
    bool stopping(uint64_t& timeout);

protected:
    void contextsResize(size_t size);
    void onTimerInsertedAtFront() override;

private:
    int m_epfd = 0;
    int m_tickleFds[2];

    std::atomic<size_t> m_pendingEventCount = {0};
    RWMutexType m_mutex;
    std::vector<FdContext*> m_fdContexts;
};
}  // namespace sylar