#include "config.h"

#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>
#include <yaml-cpp/yaml.h>

#include <iostream>
#include <list>
#include <type_traits>

#include "config.h"
#include "log.h"
namespace sylar {

//"A.B",10
// A:
// B: 10
// C: str

static void ListAllMember(
    const std::string& prefix, const YAML::Node& node,
    std::list<std::pair<std::string, const YAML::Node>>& output) {
    if (prefix.find_first_not_of("abcdefghijklmnopqrstuvwxyvz._0123456789") !=
        std::string::npos) {
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT())
            << "Config invalid name: " << prefix << " : " << node;
        return;
    }
    output.push_back(std::make_pair(prefix, node));
    if (node.IsMap()) {
        for (auto it = node.begin(); it != node.end(); ++it) {
            ListAllMember(prefix.empty() ? it->first.Scalar()
                                         : prefix + "." + it->first.Scalar(),
                          it->second, output);
        }
    }
}
void Config::LoadFromYaml(const YAML::Node& root) {
    std::list<std::pair<std::string, const YAML::Node>> all_nodes;
    ListAllMember("", root, all_nodes);
    for (auto& i : all_nodes) {
        std::string key = i.first;
        if (key.empty()) {
            continue;
        }
        std::transform(key.begin(), key.end(), key.begin(),
                       [](unsigned char c) { return std::tolower(c); });
        ConfigVarbase::ptr var = LookupBase(key);
        if (var) {
            std::cout << var->getName() << std::endl;
            if (i.second.IsScalar()) {
                var->fromString(i.second.Scalar());
            } else {
                std::stringstream ss;
                ss << i.second;
                var->fromString(ss.str());
            }
        }
    }
}
ConfigVarbase::ptr Config::LookupBase(const std::string& name) {
    auto it = m_datas.find(name);
    return it == m_datas.end() ? nullptr : it->second;
}

void Config::Visit(std::function<void(ConfigVarbase::ptr)> cb) {
    RWMutexType::ReadLock lock(s_mutex);
    configVarMap& m = m_datas;
    for (auto it = m.begin(); it != m.end(); ++it) {
        cb(it->second);
    }
}

}  // namespace sylar