#pragma once
#include <string.h>

#include "log.h"
#define SYLAR_ASSERT(X)                                   \
    if (!(X)) {                                           \
        SYLAR_LOG_ERROR(SYLAR_LOG_ROOT())                 \
            << " ASSERTION: " #X << "nbacktrace:\n"       \
            << sylar::BackstraceToString(100, 2, "    "); \
        assert(X);                                        \
    }

#define SYLAR_ASSERT2(X, W)                                 \
    if (!(X)) {                                             \
        SYLAR_LOG_ERROR(SYLAR_LOG_ROOT())                   \
            << " ASSERTION: " #X "\n" #W << "nbacktrace:\n" \
            << sylar::BackstraceToString(100, 2, "    ");   \
        assert(X);                                          \
    }
