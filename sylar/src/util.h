#pragma once
#include <sched.h>

#include <cstdint>
#include <string>
#include <vector>
namespace sylar {
pid_t GetThreadId();
uint32_t GetFiberId();
void Backtrace(std::vector<std::string>& bt, int size, int skip);
std::string BackstraceToString(int size = 64, int skip = 1,
                               const std::string& prefix = "");
// 时间ms
uint64_t GetCurrentMS();
uint64_t GetCurrentUS();
}  // namespace sylar