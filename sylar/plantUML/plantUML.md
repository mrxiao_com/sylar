@startuml logger
class Logger{
-name:string
-level:LogLevel
-appenders:std::vector<LogAppender>
-formatter:LogFormatter
+log()
+debug()
+info()
+warn()
+error()
+fatal()
+addAppender()
+delAppender()
+clearAppender()
+setFormatter()
+setLevel()
}
class LogAppender{
-level:LogLevel
-formatter:LogFormatter
+log()
+toYamlString()
}

class LogFormatter{
-formatter:string
-items:std::vector<FormatterItem>
+format()
}

class FileLogAppender{
-file:string
+log()
+toYamlString()
}

class StdoutLogAppender{
-file:string
+log()
+toYamlString()
}

interface FormatItem{
+{abstract}format()
}

class MessageFormatItem{
+format()
} 
class LevelFormatItem{
+format()
}
class ElapseFormatItem{
+format()
}  
class NameFormatItem{
+format()
}
class ThreadIdFormatItem{
+format()
}
class NewLineFormatItem{
+format()
}
class DataTimeFormatItem{
+format()
}
class FileNameFormatItem{
+format()
}
class LineFormatItem{
+format()
}
class TabFormatItem{
+format()
}
class FiberIdFormatItem{
+format()
}


Logger -->"1..N" LogAppender
Logger -->"1..1" LogFormatter
FileLogAppender --|> LogAppender
StdoutLogAppender --|> LogAppender
LogAppender -->"1..1" LogFormatter
LogFormatter -->"1..N" FormatItem

MessageFormatItem .up.|> FormatItem 
LevelFormatItem .up.|> FormatItem
ElapseFormatItem .up.|> FormatItem  
NameFormatItem .up.|> FormatItem
ThreadIdFormatItem .left.|> FormatItem
NewLineFormatItem .left.|> FormatItem
DataTimeFormatItem .right.|> FormatItem
FileNameFormatItem .right.|> FormatItem
LineFormatItem .right.|> FormatItem
TabFormatItem .left.|> FormatItem
FiberIdFormatItem .up.|> FormatItem

@enduml


@startuml config
class ConfigVarBase{
-name:string
-description:string
+{abstract}toString() = 0:string 
+{abstract}fromString(const string& val) = 0:bool 
}

class ConfigVar <class T, class FromStr, class ToStr> {
-val:T
+fromString()
+toString()
+getValue()
}

class Config{
+configVarMap::map<std::string,ConfigVarbase::ptr>
+Lookup()
}

class LexicalCast <F,T> {
-val:T
+operation()(F):T void
}
class "LexicalCast <vector<T>,string>" <vector<T>,string> {
-val:T
operator()(const vector<T>&):string
}
class "LexicalCast <string,vector<T>>" <string,vector<T>> {
-val:T
operator()(const string&):vector<T>
}


Config --> ConfigVarBase
ConfigVar --|> ConfigVarBase
LexicalCast ..> ConfigVar : "T=T \n FromStr=F \n ToStr=T"
"LexicalCast <vector<T>,string>" ..> ConfigVar:"T=T \n FromStr=vector<T> \n ToStr=string"
"LexicalCast <string,vector<T>>" ..> ConfigVar:"T=T \n FromStr=string \n ToStr=vector<T>"
@enduml


@startuml

actor User

participant Main as mainThread
participant Fiber as f1
participant Logger as Log

User -> mainThread: start()
activate mainThread

mainThread --> Log: log("main begin -1")
mainThread -> mainThread: Fiber::GetThis()
mainThread --> Log: log("main begin")
mainThread -> f1: create Fiber(run_in_fiber)
activate f1

mainThread --> Log: log("Fiber::Fiber id0")
mainThread -> f1: f1->swapIn()
deactivate mainThread
activate f1

f1 --> Log: log("run_in_fiber begin")
f1 -> mainThread: Fiber::YieldToRead()
deactivate f1
activate mainThread

mainThread --> Log: log("main after swapIn")
mainThread -> f1: f1->swapIn()
deactivate mainThread
activate f1

f1 --> Log: log("run_in_fiber end")
f1 -> mainThread: Fiber::YieldToHold()
deactivate f1
activate mainThread

mainThread --> Log: log("main after end1")
mainThread -> f1: f1->swapIn()
deactivate mainThread
activate f1

f1 -> mainThread: swapOut()
deactivate f1
activate mainThread

mainThread --> Log: log("Fiber::~Fiber id1")
mainThread --> Log: log("main after end2")

mainThread --> Log: log("Fiber::~Fiber id0")
mainThread -> mainThread: return 0
deactivate mainThread

@enduml


@startuml
participant MainThread as MT
participant Scheduler as SC
participant Thread0 as T0
participant Thread1 as T1
participant Thread2 as T2
participant Fiber0 as F0
participant Fiber1 as F1
participant Fiber2 as F2

MT -> SC: Create Scheduler
SC -> T0: Start Thread name_0
T0 -> F0: Create Main Fiber id0
T0 -> F1: Create Fiber id1
SC -> T1: Start Thread name_1
T1 -> F0: Create Main Fiber id0
T1 -> F2: Create Fiber id2
SC -> T2: Start Thread name_2
T2 -> F0: Create Main Fiber id0
T2 -> F2: Create Fiber id3

T0 -> T0: Idle
T1 -> T1: Idle
T2 -> T2: Idle

MT -> SC: Add test_fiber to schedule
SC -> T0: Wake up Thread name_0
T0 -> F1: Swap in Fiber id1 (test_fiber)
F1 -> F1: Execute test_fiber, s_count=5
F1 -> F1: YieldToRead, s_count=4
T0 -> T0: Swap out Fiber id1
T0 -> T0: Idle

SC -> T1: Wake up Thread name_1
T1 -> F2: Swap in Fiber id2 (test_fiber)
F2 -> F2: Execute test_fiber, s_count=4
F2 -> F2: YieldToRead, s_count=3
T1 -> T1: Swap out Fiber id2
T1 -> T1: Idle

SC -> T0: Wake up Thread name_0
T0 -> F1: Swap in Fiber id1 (test_fiber)
F1 -> F1: Execute test_fiber, s_count=3
F1 -> F1: YieldToRead, s_count=2
T0 -> T0: Swap out Fiber id1
T0 -> T0: Idle

SC -> T0: Wake up Thread name_0
T0 -> F1: Swap in Fiber id1 (test_fiber)
F1 -> F1: Execute test_fiber, s_count=2
F1 -> F1: YieldToRead, s_count=1
T0 -> T0: Swap out Fiber id1
T0 -> T0: Idle

SC -> T2: Wake up Thread name_2
T2 -> F2: Swap in Fiber id3 (test_fiber)
F2 -> F2: Execute test_fiber, s_count=1
F2 -> F2: YieldToRead, s_count=0
T2 -> T2: Swap out Fiber id3
T2 -> T2: Idle

T2 -> F2: Destroy Fiber id3
T1 -> F2: Destroy Fiber id2
T0 -> F1: Destroy Fiber id1

MT -> SC: Stop Scheduler
SC -> T0: Stop Thread name_0
T0 -> F0: Destroy Main Fiber id0
SC -> T1: Stop Thread name_1
T1 -> F0: Destroy Main Fiber id0
SC -> T2: Stop Thread name_2
T2 -> F0: Destroy Main Fiber id0
MT -> MT: Main exits
@enduml


@startuml
participant Main
participant Scheduler
participant Fiber
participant "Fiber::MainFunc" as FiberMainFunc

Main -> Main: "start"
Main -> Scheduler: "Scheduler sc(3, false, 'name')"
Scheduler -> Scheduler: "start"
Scheduler -> Fiber: "create main fiber (id=0)"
Fiber -> Fiber: "set state to EXEC"
Scheduler -> Fiber: "create worker fiber (id=1)"
Fiber -> Fiber: "initialize context"
Scheduler -> Fiber: "create worker fiber (id=2)"
Fiber -> Fiber: "initialize context"
Scheduler -> Fiber: "create worker fiber (id=3)"
Fiber -> Fiber: "initialize context"
Fiber --> Scheduler: "return"
Main -> Main: "sleep(2)"
Main -> Scheduler: "schedule test_fiber"
Scheduler -> Fiber: "create test_fiber (id=4)"
Fiber -> Fiber: "initialize context"
Fiber -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=5"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=5)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=4"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=6)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=3"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=7)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=2"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=8)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=1"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=9)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=0"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "idle fiber term"
Scheduler -> Fiber: "destroy fiber (id=5)"
Fiber -> Fiber: "cleanup"
Scheduler -> Fiber: "destroy fiber (id=4)"
Fiber -> Fiber: "cleanup"
Scheduler -> Fiber: "destroy fiber (id=6)"
Fiber -> Fiber: "cleanup"
Scheduler -> Fiber: "destroy fiber (id=1)"
Fiber -> Fiber: "cleanup"
Scheduler -> Fiber: "destroy fiber (id=3)"
Fiber -> Fiber: "cleanup"
Scheduler -> Fiber: "destroy fiber (id=0)"
Fiber -> Fiber: "cleanup"
Scheduler -> Fiber: "destroy fiber (id=2)"
Fiber -> Fiber: "cleanup"
Scheduler -> Fiber: "destroy fiber (id=0)"
Fiber -> Fiber: "cleanup"
Scheduler -> Fiber: "destroy fiber (id=0)"
Fiber -> Fiber: "cleanup"
Scheduler -> Scheduler: "stop"
Main -> Main: "over"
@enduml

@startuml
participant Main
participant Scheduler
participant Fiber
participant "Fiber::MainFunc" as FiberMainFunc

Main -> Main: "start"
Main -> Scheduler: "Scheduler sc(3, false, 'name')"
activate Scheduler
Scheduler -> Scheduler: "start"
Scheduler -> Fiber: "create main fiber (id=0)"
activate Fiber
Fiber -> Fiber: "set state to EXEC"
deactivate Fiber
Scheduler -> Fiber: "create worker fiber (id=1)"
activate Fiber
Fiber -> Fiber: "initialize context"
deactivate Fiber
Scheduler -> Fiber: "create worker fiber (id=2)"
activate Fiber
Fiber -> Fiber: "initialize context"
deactivate Fiber
Scheduler -> Fiber: "create worker fiber (id=3)"
activate Fiber
Fiber -> Fiber: "initialize context"
deactivate Fiber
Fiber --> Scheduler: "return"
deactivate Scheduler

Main -> Main: "sleep(2)"
Main -> Scheduler: "schedule test_fiber"
activate Scheduler
Scheduler -> Fiber: "create test_fiber (id=4)"
activate Fiber
Fiber -> Fiber: "initialize context"
deactivate Fiber
Fiber -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=5"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=5)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=4"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=6)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=3"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=7)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=2"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=8)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=1"
FiberMainFunc -> Main: "sleep(1)"
FiberMainFunc -> Scheduler: "schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "tickle"
Scheduler -> FiberMainFunc: "swapIn (Fiber::MainFunc, id=9)"
activate FiberMainFunc
FiberMainFunc -> Main: "test in fiber s_count=0"
FiberMainFunc -> FiberMainFunc: "swapOut"
deactivate FiberMainFunc
FiberMainFunc -> Scheduler: "idle fiber term"
Scheduler -> Fiber: "destroy fiber (id=5)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Fiber: "destroy fiber (id=4)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Fiber: "destroy fiber (id=6)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Fiber: "destroy fiber (id=1)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Fiber: "destroy fiber (id=3)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Fiber: "destroy fiber (id=0)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Fiber: "destroy fiber (id=2)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Fiber: "destroy fiber (id=0)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Fiber: "destroy fiber (id=0)"
activate Fiber
Fiber -> Fiber: "cleanup"
deactivate Fiber
Scheduler -> Scheduler: "stop"
deactivate Scheduler
Main -> Main: "over"
@enduml




' xiaqiu@xz  ~/sylar/build/sylar  /home/xiaqiu/sylar/build/sylar/day1
' 2024-06-25 17:24:55     27050   [UNKNOW ]       0       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:21       INFO main
' 2024-06-25 17:24:55     27051   [name_0]        0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:120  INFO run
' 2024-06-25 17:24:55     27051   [name_0]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:40       DEBUG Fiber::Fiber id0
' 2024-06-25 17:24:55     27051   [name_0]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:60       DEBUG Fiber::Fiber id1
' 2024-06-25 17:24:55     27052   [name_1]        0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:120  INFO run
' 2024-06-25 17:24:55     27052   [name_1]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:40       DEBUG Fiber::Fiber id0
' 2024-06-25 17:24:55     27052   [name_1]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:60       DEBUG Fiber::Fiber id2
' 2024-06-25 17:24:55     27053   [name_2]        0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:120  INFO run
' 2024-06-25 17:24:55     27053   [name_2]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:40       DEBUG Fiber::Fiber id0
' 2024-06-25 17:24:55     27053   [name_2]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:60       DEBUG Fiber::Fiber id3
' 2024-06-25 17:24:55     27052   [name_1]        2       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:217  INFO idle
' 2024-06-25 17:24:55     27053   [name_2]        3       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:217  INFO idle
' 2024-06-25 17:24:55     27051   [name_0]        1       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:217  INFO idle
' 2024-06-25 17:24:57     27050   [UNKNOW ]       0       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:25       INFO scheduler
' 2024-06-25 17:24:57     27050   [UNKNOW ]       0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:24:57     27050   [UNKNOW ]       0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:24:57     27050   [UNKNOW ]       0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:24:57     27050   [UNKNOW ]       0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:24:57     27051   [name_0]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:60       DEBUG Fiber::Fiber id4
' 2024-06-25 17:24:57     27051   [name_0]        4       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:11       INFO test in fiber s_count=5
' 2024-06-25 17:24:58     27051   [name_0]        4       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:24:58     27052   [name_1]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:60       DEBUG Fiber::Fiber id5
' 2024-06-25 17:24:58     27052   [name_1]        5       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:11       INFO test in fiber s_count=4
' 2024-06-25 17:24:59     27052   [name_1]        5       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:24:59     27051   [name_0]        4       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:11       INFO test in fiber s_count=3
' 2024-06-25 17:25:00     27051   [name_0]        4       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:25:00     27051   [name_0]        4       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:11       INFO test in fiber s_count=2
' 2024-06-25 17:25:01     27051   [name_0]        4       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:25:01     27053   [name_2]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:60       DEBUG Fiber::Fiber id6
' 2024-06-25 17:25:01     27053   [name_2]        6       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:11       INFO test in fiber s_count=1
' 2024-06-25 17:25:02     27053   [name_2]        6       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:209  INFO tickle
' 2024-06-25 17:25:02     27053   [name_2]        6       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:11       INFO test in fiber s_count=0
' 2024-06-25 17:25:03     27052   [name_1]        0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:194  INFO idle fiber term
' 2024-06-25 17:25:03     27051   [name_0]        0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:194  INFO idle fiber term
' 2024-06-25 17:25:03     27053   [name_2]        0       [INFO]  [system]        /home/xiaqiu/sylar/sylar/src/scheduler.cpp:194  INFO idle fiber term
' 2024-06-25 17:25:03     27052   [name_1]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id5
' 2024-06-25 17:25:03     27051   [name_0]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id4
' 2024-06-25 17:25:03     27053   [name_2]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id6
' 2024-06-25 17:25:03     27051   [name_0]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id1
' 2024-06-25 17:25:03     27053   [name_2]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id3
' 2024-06-25 17:25:03     27051   [name_0]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id0
' 2024-06-25 17:25:03     27052   [name_1]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id2
' 2024-06-25 17:25:03     27053   [name_2]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id0
' 2024-06-25 17:25:03     27052   [name_1]        0       [DEBUG] [system]        /home/xiaqiu/sylar/sylar/src/fiber.cpp:77       DEBUG Fiber::~Fiber id0
' 2024-06-25 17:25:03     27050   [UNKNOW ]       0       [INFO]  [root]  /home/xiaqiu/sylar/sylar/src/test_schedule.cpp:28       INFO over
'  xiaqiu@xz  ~/sylar/build/sylar  

@startuml
actor Main as main
participant Scheduler as scheduler
participant Fiber
participant FiberMainFunc

main -> main: "Start"
main -> scheduler: "Create Scheduler sc(3, false, 'name')"
activate scheduler
scheduler -> scheduler: "start()"
scheduler -> Fiber: "Create main fiber (id=0)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc -> FiberMainFunc: "Set state to EXEC"
deactivate Fiber
deactivate FiberMainFunc
scheduler -> Fiber: "Create worker fiber (id=1)"

activate Fiber
Fiber -> FiberMainFunc: "Initialize context"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Create worker fiber (id=2)"
activate Fiber
Fiber -> FiberMainFunc: "Initialize context"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Create worker fiber (id=3)"
activate Fiber
Fiber -> FiberMainFunc: "Initialize context"
deactivate FiberMainFunc
deactivate Fiber
Fiber --> scheduler: "Return"
deactivate scheduler

main -> main: "sleep(2)"
main -> scheduler: "Schedule test_fiber"
activate scheduler
scheduler -> Fiber: "Create test_fiber (id=4)"
activate Fiber
Fiber -> FiberMainFunc: "Initialize context"
deactivate FiberMainFunc
deactivate Fiber
Fiber -> scheduler: "Tickle"
scheduler -> Fiber: "SwapIn (Fiber::MainFunc)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc -> main: "Test in fiber s_count=5"
FiberMainFunc -> main: "sleep(1)"
FiberMainFunc -> scheduler: "Schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "SwapOut"
deactivate FiberMainFunc
deactivate Fiber
Fiber -> scheduler: "Tickle"
scheduler -> Fiber: "SwapIn (Fiber::MainFunc, id=5)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc -> main: "Test in fiber s_count=4"
FiberMainFunc -> main: "sleep(1)"
FiberMainFunc -> scheduler: "Schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "SwapOut"
deactivate FiberMainFunc
deactivate Fiber
Fiber -> scheduler: "Tickle"
scheduler -> Fiber: "SwapIn (Fiber::MainFunc, id=6)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc -> main: "Test in fiber s_count=3"
FiberMainFunc -> main: "sleep(1)"
FiberMainFunc -> scheduler: "Schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "SwapOut"
deactivate FiberMainFunc
deactivate Fiber
Fiber -> scheduler: "Tickle"
scheduler -> Fiber: "SwapIn (Fiber::MainFunc, id=7)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc -> main: "Test in fiber s_count=2"
FiberMainFunc -> main: "sleep(1)"
FiberMainFunc -> scheduler: "Schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "SwapOut"
deactivate FiberMainFunc
deactivate Fiber
Fiber -> scheduler: "Tickle"
scheduler -> Fiber: "SwapIn (Fiber::MainFunc, id=8)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc -> main: "Test in fiber s_count=1"
FiberMainFunc -> main: "sleep(1)"
FiberMainFunc -> scheduler: "Schedule test_fiber"
FiberMainFunc -> FiberMainFunc: "SwapOut"
deactivate FiberMainFunc
deactivate Fiber
Fiber -> scheduler: "Tickle"
scheduler -> Fiber: "SwapIn (Fiber::MainFunc, id=9)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc -> main: "Test in fiber s_count=0"
FiberMainFunc -> FiberMainFunc: "SwapOut"
deactivate FiberMainFunc
deactivate Fiber
Fiber -> scheduler: "Idle fiber term"
scheduler -> Fiber: "Destroy fiber (id=5)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Destroy fiber (id=4)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Destroy fiber (id=6)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Destroy fiber (id=1)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Destroy fiber (id=3)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Destroy fiber (id=0)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Destroy fiber (id=2)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Destroy fiber (id=0)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> Fiber: "Destroy fiber (id=0)"
activate Fiber
Fiber -> FiberMainFunc: "Cleanup"
deactivate FiberMainFunc
deactivate Fiber
scheduler -> scheduler: "Stop"
deactivate scheduler
main -> main: "Over"

@enduml


@startuml
actor Main as main
participant Scheduler as scheduler
participant Fiber
participant FiberMainFunc

main -> main: "Start"
main -> scheduler: "Create Scheduler sc(3, false, 'name')"
activate scheduler

scheduler -> Scheduler: "start()"
activate Scheduler

Scheduler -> Scheduler: "Create root fiber"
Scheduler -> Fiber: "Create main fiber (id=1)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc --> Fiber: Done execution

Scheduler -> Scheduler: "Create thread1 for fibers"
Scheduler -> Scheduler: "Create thread2 for fibers"
Scheduler -> Scheduler: "Create thread3 for fibers"

main -> scheduler: "Sleep 2 seconds"
deactivate scheduler
main -> main: "Awaken"
main -> scheduler: "Schedule test_fiber"
activate scheduler
scheduler -> Fiber: "Create test_fiber (id=2)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc --> Fiber: Done execution
deactivate scheduler

main -> scheduler: "Stop scheduler"
scheduler -> Scheduler: "stop()"
Scheduler -> Scheduler: "Stopping flag set"
Scheduler -> Scheduler: "Tickle all threads"
Scheduler -> Scheduler: "Root fiber exists, call it"
Scheduler -> Fiber: "Create root fiber (id=0)"
activate Fiber
Fiber -> FiberMainFunc: "Fiber::MainFunc"
activate FiberMainFunc
FiberMainFunc --> Fiber: Done execution
Scheduler -> Thread: "Join all threads"
deactivate scheduler

main -> main: "End"

@enduml

@startuml
participant Application
participant Scheduler
participant Thread_0
participant Thread_1
participant Thread_2
participant Fiber_0
participant Fiber_1
participant Fiber_2
participant Fiber_3

Application -> Scheduler: create Scheduler(3 threads)
Application -> Scheduler: start()

Thread_0 -> Scheduler: run()
Thread_1 -> Scheduler: run()
Thread_2 -> Scheduler: run()

activate Thread_0
activate Thread_1
activate Thread_2

Thread_0 -> Fiber_0: create Fiber id=0
Thread_1 -> Fiber_0: create Fiber id=0
Thread_2 -> Fiber_0: create Fiber id=0

Fiber_0 -> Thread_0: Fiber::Fiber id=0
Fiber_0 -> Thread_1: Fiber::Fiber id=0
Fiber_0 -> Thread_2: Fiber::Fiber id=0

Thread_0 -> Scheduler: schedule(Fiber_0)
Thread_1 -> Scheduler: schedule(Fiber_0)
Thread_2 -> Scheduler: schedule(Fiber_0)

Fiber_0 -> Thread_0: Fiber::Fiber id=1
Fiber_0 -> Thread_1: Fiber::Fiber id=2
Fiber_0 -> Thread_2: Fiber::Fiber id=3

Thread_0 -> Scheduler: idle()
Thread_1 -> Scheduler: idle()
Thread_2 -> Scheduler: idle()

Scheduler -> Application: sleep(2 seconds)

Application -> Scheduler: schedule(test_fiber)

Scheduler -> Thread_0: run()
activate Fiber_1
Thread_0 -> Fiber_1: create Fiber id=1
Fiber_1 -> Thread_0: Fiber::Fiber id=1

Thread_0 -> Scheduler: schedule(Fiber_1)
Fiber_1 -> Thread_0: Fiber::Fiber id=4

Thread_0 -> Fiber_1: Fiber::Fiber id=4
Fiber_1 -> Thread_0: Fiber::Fiber id=5

Thread_0 -> Scheduler: idle()

Scheduler -> Thread_1: run()
activate Fiber_2
Thread_1 -> Fiber_2: create Fiber id=2
Fiber_2 -> Thread_1: Fiber::Fiber id=2

Thread_1 -> Scheduler: schedule(Fiber_2)
Fiber_2 -> Thread_1: Fiber::Fiber id=6

Thread_1 -> Fiber_2: Fiber::Fiber id=6
Fiber_2 -> Thread_1: Fiber::Fiber id=7

Thread_1 -> Scheduler: idle()

Scheduler -> Thread_2: run()
activate Fiber_3
Thread_2 -> Fiber_3: create Fiber id=3
Fiber_3 -> Thread_2: Fiber::Fiber id=3

Thread_2 -> Scheduler: schedule(Fiber_3)
Fiber_3 -> Thread_2: Fiber::Fiber id=8

Thread_2 -> Fiber_3: Fiber::Fiber id=8
Fiber_3 -> Thread_2: Fiber::Fiber id=9

Thread_2 -> Scheduler: idle()

Scheduler -> Application: sleep(2 seconds)

Scheduler -> Thread_0: run()
Thread_0 -> Fiber_1: Fiber::Fiber id=5
Fiber_1 -> Thread_0: Fiber::Fiber id=10

Thread_0 -> Scheduler: schedule(Fiber_1)
Fiber_1 -> Thread_0: Fiber::Fiber id=11

Thread_0 -> Fiber_1: Fiber::Fiber id=11
Fiber_1 -> Thread_0: Fiber::Fiber id=12

Thread_0 -> Scheduler: idle()

Scheduler -> Thread_1: run()
Thread_1 -> Fiber_2: Fiber::Fiber id=7
Fiber_2 -> Thread_1: Fiber::Fiber id=13

Thread_1 -> Scheduler: schedule(Fiber_2)
Fiber_2 -> Thread_1: Fiber::Fiber id=14

Thread_1 -> Fiber_2: Fiber::Fiber id=14
Fiber_2 -> Thread_1: Fiber::Fiber id=15

Thread_1 -> Scheduler: idle()

Scheduler -> Thread_2: run()
Thread_2 -> Fiber_3: Fiber::Fiber id=9
Fiber_3 -> Thread_2: Fiber::Fiber id=16

Thread_2 -> Scheduler: schedule(Fiber_3)
Fiber_3 -> Thread_2: Fiber::Fiber id=17

Thread_2 -> Fiber_3: Fiber::Fiber id=17
Fiber_3 -> Thread_2: Fiber::Fiber id=18

Thread_2 -> Scheduler: idle()

Scheduler -> Application: stop()

Application -> Scheduler: stop()

Scheduler -> Thread_0: run()
Thread_0 -> Fiber_1: Fiber::Fiber id=12
Fiber_1 -> Thread_0: Fiber::Fiber id=19

Thread_0 -> Scheduler: schedule(Fiber_1)
Fiber_1 -> Thread_0: Fiber::Fiber id=20

Thread_0 -> Fiber_1: Fiber::Fiber id=20
Fiber_1 -> Thread_0: Fiber::Fiber id=21

Thread_0 -> Scheduler: idle()

Scheduler -> Thread_1: run()
Thread_1 -> Fiber_2: Fiber::Fiber id=15
Fiber_2 -> Thread_1: Fiber::Fiber id=22

Thread_1 -> Scheduler: schedule(Fiber_2)
Fiber_2 -> Thread_1: Fiber::Fiber id=23

Thread_1 -> Fiber_2: Fiber::Fiber id=23
Fiber_2 -> Thread_1: Fiber::Fiber id=24

Thread_1 -> Scheduler: idle()

Scheduler -> Thread_2: run()
Thread_2 -> Fiber_3: Fiber::Fiber id=18
Fiber_3 -> Thread_2: Fiber::Fiber id=25

Thread_2 -> Scheduler: schedule(Fiber_3)
Fiber_3 -> Thread_2: Fiber::Fiber id=26

Thread_2 -> Fiber_3: Fiber::Fiber id=26
Fiber_3 -> Thread_2: Fiber::Fiber id=27

Thread_2 -> Scheduler: idle()

Thread_0 -> Fiber_1: Fiber::~Fiber id=21
Thread_1 -> Fiber_2: Fiber::~Fiber id=24
Thread_2 -> Fiber_3: Fiber::~Fiber id=27

Scheduler -> Thread_0: run()
Thread_0 -> Fiber_1: Fiber::~Fiber id=19
Thread_1 -> Fiber_2: Fiber::~Fiber id=22
Thread_2 -> Fiber_3: Fiber::~Fiber id=25

Thread_0 -> Scheduler: idle()

Scheduler -> Thread_1: run()
Thread_1 -> Fiber_2: Fiber::~Fiber id=23
Thread_2 -> Fiber_3: Fiber::~Fiber id=26

Thread_1 -> Scheduler: idle()

Scheduler -> Thread_2: run()
Thread_2 -> Fiber_3: Fiber::~Fiber id=26

Thread_2 -> Scheduler: idle()

Thread_0 -> Scheduler: run()
Thread_1 -> Scheduler: run()
Thread_2 -> Scheduler: run()

deactivate Thread_0
deactivate Thread_1
deactivate Thread_2

@enduml
